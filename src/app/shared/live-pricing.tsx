import CryptocurrencyPricingTable from '@/components/cryptocurrency-pricing-table/cryptocurrency-pricing-table';
import TransactionTable from '@/components/transaction/transaction-table';
import LivePricingSlider from '@/components/ui/live-pricing-slider';

export default function LiveDemo() {
  return (
    <>
      <LivePricingSlider limits={4} />
      <TransactionTable />
    </>
  );
}
