'use client';

import { useContext, useState } from 'react';
import cn from 'classnames';
import Button from '@/components/ui/button';
import CoinInput from '@/components/ui/coin-input';
import TransactionInfo from '@/components/ui/transaction-info';
import { SwapIcon } from '@/components/icons/swap-icon';
import WalletBox from '@/components/ui/wallet-box';
import Web3Context from '@/store/web3-context';
import { airdrop } from '@/shared/api/airdrop';
import MyTransactionTable from '@/components/transaction/my-transaction-table';
import { useModal } from '@/components/modal-views/context';

const AirdropPage = () => {
  const web3Ctx = useContext(Web3Context);
  const { openModal } = useModal();

  const clickHandler = async () => {
    if(web3Ctx.data) {
      airdrop(web3Ctx.data.wallet);
    } else {
      openModal('SIGNIN_MODAL');
    }
  }

  return (
    <>
      <WalletBox>
        <div className="pt-5 mb-5 border-b border-dashed border-gray-200 pb-5 dark:border-gray-800 xs:mb-7 xs:pb-6">
          <p className='text-center'>Airdrop starged at Aug 3.</p>
          <p className='text-center'>9995 airdrop remains</p>
          <br/>
          <p className='text-center'>Airdroop 4 SHE($20)</p>
          <p className='text-center'>1SHE = $5</p>
        </div>
        <Button
          size="large"
          shape="rounded"
          fullWidth={true}
          className="uppercase xs:tracking-widest"
          onClick={clickHandler}
        >
          {web3Ctx.data ? 'Airdrop' : 'Sign In'}
        </Button>
      </WalletBox>
      {web3Ctx.account && <MyTransactionTable />}
    </>
  );
};

export default AirdropPage;
