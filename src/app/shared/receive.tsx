'use client';

import { useState, useContext } from 'react';
import cn from 'classnames';
import Button from '@/components/ui/button';
import CoinInput from '@/components/ui/coin-input';
import TransactionInfo from '@/components/ui/transaction-info';
import { SwapIcon } from '@/components/icons/swap-icon';
import Trade from '@/components/ui/trade';
import WalletBox from '@/components/ui/wallet-box';
import Web3Context from '@/store/web3-context';
import MyTransactionTable from '@/components/transaction/my-transaction-table';
import { truncateReceiveWallet } from '@/utils/helper';
import { useModal } from '@/components/modal-views/context';

const ReceivePage = () => {
  let [toggleCoin, setToggleCoin] = useState(false);
  const web3Ctx = useContext(Web3Context);
  const {openModal} = useModal();
  
  const clickHandler = async () => {
    if(web3Ctx.data) {
      navigator.clipboard.writeText(web3Ctx.data.wallet);
      alert('wallet address copied');
    } else {
      // await web3Ctx.loadAccount();
      openModal('SIGNIN_MODAL');
    }
  }

  return (
    <>
      <WalletBox>
        <div className="pt-5 mb-5 border-b border-dashed border-gray-200 pb-5 dark:border-gray-800 xs:mb-7 xs:pb-6">
          
          <p className='text-center'>{web3Ctx.data ? 'Please copy your wallet address.' : 'Please sign in'}</p>
          {web3Ctx.data && <p className='text-center pt-5'>{truncateReceiveWallet(web3Ctx.data.wallet)}</p>}
          {/* <div
            className={cn(
              'relative flex gap-3',
              toggleCoin ? 'flex-col-reverse' : 'flex-col'
            )}
          >
            <CoinInput
              label={'From'}
              exchangeRate={0.0}
              defaultCoinIndex={0}
              getCoinValue={(data) => console.log('From coin value:', data)}
            />
            <div className="absolute left-1/2 top-1/2 z-[1] -ml-4 -mt-4 rounded-full bg-white shadow-large dark:bg-gray-600">
              <Button
                size="mini"
                color="gray"
                shape="circle"
                variant="transparent"
                onClick={() => setToggleCoin(!toggleCoin)}
              >
                <SwapIcon className="h-auto w-3" />
              </Button>
            </div>
            <CoinInput
              label={'To'}
              exchangeRate={0.0}
              defaultCoinIndex={1}
              getCoinValue={(data) => console.log('To coin value:', data)}
            />
          </div> */}
        </div>
        {/* <div className="flex flex-col gap-4 xs:gap-[18px]">
          <TransactionInfo label={'Min. Received'} />
          <TransactionInfo label={'Rate'} />
          <TransactionInfo label={'Offered by'} />
          <TransactionInfo label={'Price Slippage'} value={'1%'} />
          <TransactionInfo label={'Network Fee'} />
          <TransactionInfo label={'Criptic Fee'} />
        </div> */}
        <Button
          size="large"
          shape="rounded"
          fullWidth={true}
          className="uppercase xs:tracking-widest"
          onClick={clickHandler}
        >
          {web3Ctx.data ? 'Copy Wallet Address' : 'Sign In'}
        </Button>
      </WalletBox>
      {web3Ctx.account && <MyTransactionTable />}
    </>
  );
};

export default ReceivePage;
