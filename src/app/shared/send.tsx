'use client';

import { useState, useContext } from 'react';
import cn from 'classnames';
import Button from '@/components/ui/button';
import CoinInput from '@/components/ui/coin-input';
import TransactionInfo from '@/components/ui/transaction-info';
import { SwapIcon } from '@/components/icons/swap-icon';
import WalletBox from '@/components/ui/wallet-box';
import Web3Context from '@/store/web3-context';
import { sendTransfer } from '@/shared/web3/chainInfo';
import MyTransactionTable from '@/components/transaction/my-transaction-table';
import { useModal } from '@/components/modal-views/context';
import { transfer } from '@/shared/api/user';

const SendPage = () => {
  const web3Ctx = useContext(Web3Context);
  const [toAddress, SetToAddress] = useState('');
  const [amount, SetAmount] = useState('');
  const {openModal} = useModal();

  const clickHandler = async () => {
    if(web3Ctx.data) {
      // sendTransfer(web3Ctx.account, toAddress, amount);
      await transfer({toAddress, amount});
    } else {
      // await web3Ctx.loadAccount();
      openModal('SIGNIN_MODAL');
    }
  }

  return (
    <>
      <WalletBox>
        <div className="mb-5 border-b border-dashed border-gray-200 pb-5 dark:border-gray-800 xs:mb-7 xs:pb-6">
          <div className='flex flex-row'>
            <p className='flex items-center justify-end w-20 pr-3'>To</p>
            <input 
              type="text" 
              id="accountID" 
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
              placeholder="Account ID" 
              required 
              value={toAddress}
              onChange={(e) => SetToAddress(e.target.value)}
              />
          </div>
          <br/>
          <div className='flex flex-row'>
            <p className='flex items-center justify-end w-20 pr-3'>Amount</p>
            <input type="text" 
              id="Amount" 
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
              placeholder="Amount" 
              required 
              value={amount}
              onChange={(e) => SetAmount(e.target.value)}
              />
          </div>
        </div>
        <Button
          size="large"
          shape="rounded"
          fullWidth={true}
          className="uppercase xs:tracking-widest"
          onClick={clickHandler}
        >
          {web3Ctx.data ? 'Send' : 'Sign In'}
        </Button>
      </WalletBox>
      {web3Ctx.data && <MyTransactionTable />}
    </>
  );
};

export default SendPage;
