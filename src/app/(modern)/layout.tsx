"use client"

import RetroLayout from '@/layouts/retro/layout';
import Web3Context from '@/store/web3-context';
import { useContext, useEffect } from 'react';

export default function DefaultLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const web3Ctx = useContext(Web3Context);

  // useEffect(() => {
  //   (async () => {
  //     if(!web3Ctx.account) {
  //       await web3Ctx.loadAccount();
  //     }
  //   })()
  // }, []);

  return <RetroLayout>{children}</RetroLayout>;
}
