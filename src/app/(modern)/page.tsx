'use client';

import ModernScreen from '@/components/screens/modern-screen';
import LiveDemoModern from './live-pricing/page';
import Products from '@/components/products/products';
import { useContext, useEffect, useState } from 'react';
import { useModal } from '@/components/modal-views/context';
import { isauthorized } from '@/shared/api/user';
import Web3Context from '@/store/web3-context';

const timeout = (delay: number) => {
  return new Promise( res => setTimeout(res, delay) );
}

export default function IndexPageModern() {
  const [load, setLoad] = useState(false);
  const { openModal } = useModal();
  const web3Ctx = useContext(Web3Context);

  useEffect(() => {
    let auth_token = localStorage.getItem('auth_token');
    if(!load && auth_token === null) {
      (async () => {
        await timeout(1000);
        const isLoggedIn = await isauthorized(web3Ctx);
        if(!isLoggedIn) {
          openModal('SIGNIN_MODAL');
        }
        setLoad(true);
      })()
    }
  }, [load]);
  return <Products />;
}
