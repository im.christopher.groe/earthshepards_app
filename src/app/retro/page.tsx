import LiveDemoRetro from './live-pricing/page';
import Products from '@/components/products/products';

export default function IndexPageRetro() {
  return <LiveDemoRetro />;
}
