import { Suspense } from 'react';
import { Provider } from 'react-redux' ;
import { Fira_Code } from 'next/font/google';
import cn from 'classnames';
import { QueryClientProvider } from '@/app/shared/query-client-provider';
import { ThemeProvider } from '@/app/shared/theme-provider';
import WagmiConfig from '@/app/shared/wagmi-config';
import ModalsContainer from '@/components/modal-views/container';
import DrawersContainer from '@/components/drawer-views/container';
import SettingsButton from '@/components/settings/settings-button';
import SettingsDrawer from '@/components/settings/settings-drawer';
import Web3Provider from '@/store/Web3Provider';
// base css file
import 'overlayscrollbars/overlayscrollbars.css';
import 'swiper/css';
import 'swiper/css/pagination';
import '@/assets/css/scrollbar.css';
import '@/assets/css/globals.css';
import '@/assets/css/range-slider.css';

// const fira_code = Fira_Code({
//   weight: ['400', '500', '600', '700'],
//   subsets: ['latin'],
//   display: 'swap',
// });

export const metadata = {
  title: 'Earthshepards',
  description: 'Earthshepards - React Next Web3 NFT Crypto Dashboard Template',
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en" dir="ltr" className={cn('light')}>
      <head>
        {/* maximum-scale 1 meta tag need to prevent ios input focus auto zooming */}
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1 maximum-scale=1"
        />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@700&family=Roboto&display=swap" rel="stylesheet" />
      </head>
      <body>
        <Web3Provider>
          <QueryClientProvider>
            <ThemeProvider>
              <WagmiConfig>
                
                <SettingsButton />
                <SettingsDrawer />
                <Suspense fallback={null}>
                  <ModalsContainer />
                  <DrawersContainer />
                </Suspense>
                {children}
              </WagmiConfig>
            </ThemeProvider>
          </QueryClientProvider>
        </Web3Provider>
      </body>
    </html>
  );
}
