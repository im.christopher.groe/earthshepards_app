import axios from 'axios';
import { BACKEND_API_URL } from "@/lib/constants";

export const uploadFile = async (body: any) => {
    try {
        const res = await axios.post(BACKEND_API_URL + 'upload/single', body);
        return res.data.url;
    } catch (error: any) {
        console.log(error);
        return '';
    }
}