import axios from 'axios';
import { BACKEND_API_URL } from "@/lib/constants";

export const getUserInfo = async (wallet: string) => {
    try {
        const res = await axios.post(BACKEND_API_URL + 'user/getUserByWallet', {wallet});
        return res.data.data.data;
        // alert(res.data.message);
    } catch (error: any) {
        console.log(error);
        return null;
    }
}

export const getCompanies = async () => {
    try {
        const res = await axios.post(BACKEND_API_URL + 'user/filter', { role: 'Company' });
        return res.data.data.data;
        // alert(res.data.message);
    } catch (error: any) {
        console.log(error);
        return null;
    }
}

export const signin = async (data: any, web3Ctx: any) => {
    try {
        const res = await axios.post(BACKEND_API_URL + 'auth/login', data);
        web3Ctx.setUserData(res.data.data.user);
        localStorage.setItem('auth_token', res.data.token);
        // web3Ctx.setAuthToken(res.data.token);
        return true;
    } catch (error: any) {
        console.log(error);
        alert(error.response.data.message);
        return false;
    }
}

export const register = async (data: any, web3Ctx: any) => {
    try {
        const res = await axios.post(BACKEND_API_URL + 'auth/signup', data);
        web3Ctx.setUserData(res.data.data.user);
        localStorage.setItem('auth_token', res.data.token);
        // web3Ctx.setAuthToken(res.data.token);
        // alert('Join whitelist request sent successfully!!!');
        // alert('Successfully registered!!! Please try to login.');
        return true;
    } catch (error: any) {
        console.log(error);
        return false;
    }
}

export const updateProfile = async (id: any, data: any) => {
    try {
        const res = await axios.put(BACKEND_API_URL + 'user/' + id, data);
        alert('Successfully Updated!!!');
        return true;
    } catch (error: any) {
        console.log(error);
        return false;
    }
}

export const transfer = async (data: any) => {
    try {
        let auth_token = localStorage.getItem('auth_token');
        const res = await axios.post(BACKEND_API_URL + 'user/send', data, {
            headers: { Authorization: `Bearer ` + auth_token }
        });
        alert('Successfully sent');
        return true;
    } catch (error: any) {
        console.log(error);
        alert(error.response.data.message);
        return false;
    }
}

export const isauthorized = async (web3Ctx: any) => {
    try {
        let auth_token = localStorage.getItem('auth_token');
        const res = await axios.post(BACKEND_API_URL + 'user/isauthorized', {}, {
            headers: { Authorization: `Bearer ` + auth_token }
        });
        web3Ctx.setUserData(res.data.data);
        console.log('Success get user data');
        return true;
    } catch (error: any) {
        console.log(error);
        return false;
    }
}