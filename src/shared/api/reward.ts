import axios from 'axios';
import { BACKEND_API_URL } from "@/lib/constants";

export const rewardRequest = async (data: any) => {
    try {
        await axios.post(BACKEND_API_URL + 'reward/', data);
        // alert(res.data.message);
        alert('Successfully Requested!!!');
    } catch (error: any) {
        console.log(error);
    }
}

export const getRewardRequestListWithFilter = async (filter: any) => {
    try {
        const res = await axios.post(BACKEND_API_URL + 'reward/filter', filter);
        return res.data.data.data;
        // alert(res.data.message);
    } catch (error: any) {
        console.log(error);
        return [];
    }
}

export const updateReward = async (id: any, data: any) => {
    try {
        await axios.put(BACKEND_API_URL + 'reward/' + id, data);
        // alert(res.data.message);
    } catch (error: any) {
        console.log(error);
    }
}