import axios from 'axios';
import { AIRDROP_API_URL } from "@/lib/constants";

export const airdrop = async (to: string) => {
    try {
        await axios.post(AIRDROP_API_URL, { wallet: to });
        alert('Successfully Airdropped');
        // alert(res.data.message);
    } catch (error: any) {
        console.log(error);
        alert('Airdrop Failed: ' + error.response.data.message);
    }
}