import axios from 'axios';
import { BACKEND_API_URL } from "@/lib/constants";

export const getProducts = async () => {
    try {
        const res = await axios.get(BACKEND_API_URL + 'product/');
        return res.data.data.data;
        // alert(res.data.message);
    } catch (error: any) {
        console.log(error);
        return [];
    }
}

export const getProductsWithFilter = async (filter: any) => {
    try {
        const res = await axios.post(BACKEND_API_URL + 'product/filter', filter);
        return res.data.data.data;
        // alert(res.data.message);
    } catch (error: any) {
        console.log(error);
        return [];
    }
}

export const createProduct = async (data: any) => {
    try {
        const res = await axios.post(BACKEND_API_URL + 'product/', data);
        alert("Successfully Created!!! Please wait for Admin approve this.");
        return true;
    } catch (error: any) {
        console.log(error);
        return false;
    }
}

export const updateProduct = async (id: any, data: any) => {
    try {
        await axios.put(BACKEND_API_URL + 'product/' + id, data);
        // alert(res.data.message);
    } catch (error: any) {
        console.log(error);
    }
}
