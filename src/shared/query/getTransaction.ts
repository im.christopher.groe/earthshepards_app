
import { ApolloClient, InMemoryCache, gql } from "@apollo/client";
import { graphqlApiURL } from '../../lib/constants';

const client = new ApolloClient({
    uri: graphqlApiURL,
    cache: new InMemoryCache(),
    defaultOptions: {
      query: {
        fetchPolicy: 'no-cache',
        errorPolicy: 'all',
      }
    }
    // credentials: 'include'
});

export const allTransactionQuery = client.query({
    query: gql`
      query {
        getExtrinsics(
          filters: {
            callModule: "Balances"
            or: [
              { callName: "transfer" }
              { callName: "transfer_keep_alive" }
              { callName: "transfer_all" }
            ]
          }
          pageSize: 10
        ) {
          objects {
            blockNumber
            extrinsicIdx
            hash
            callModule
            callName
            signed
            blockHash
            blockDatetime
            multiAddressAccountId
            callArguments
          }
          pageInfo {
            pageSize
            pageNext
            pagePrev
          }
        }
      }
    `,
});