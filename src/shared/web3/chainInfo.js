import { ApiPromise, WsProvider } from '@polkadot/api';

import { websocketURL } from '../../lib/constants';

const wsProvider = new WsProvider(websocketURL);

export const getCurrentBlock = async (setCurrentBlock) => {
    const api = await ApiPromise.create({ provider: wsProvider });
    await api.rpc.chain.subscribeNewHeads((lastHeader) => {
        setCurrentBlock(lastHeader.number.toNumber());
    })
}

export const getFinalizedBlock = async (setFinalizedBlock) => {
    const api = await ApiPromise.create({ provider: wsProvider });
    await api.rpc.chain.subscribeFinalizedHeads((finalizedHead) => {
        setFinalizedBlock(finalizedHead.number.toNumber());
    })
}

export const txResHandler = async ({ events = [], status, txHash }) =>{
    
    console.log(status.isFinalized ? `😉 Finalized. Block hash: ${status.asFinalized.toString()}` : `Current transaction status: ${status.type}`)

      // Loop through Vec<EventRecord> to display all events
    const api = await ApiPromise.create({ provider: wsProvider });

    events.forEach( async ({ _, event: { data, method, section } }) => {
        if ((section + ":" + method) === 'system:ExtrinsicFailed' ) {
        } else if (section + ":" + method === 'system:ExtrinsicSuccess' ) {
            console.log(`❤️️ Transaction successful! tx hash: ${txHash}`)

        //   getBalance(String(actualAccount?.address));
        //   setLoading(false);
        //   setHandlerFaucetModal(false);
        //   setHandlerSendModal(false);

            
        }
    });
  }

export const sendTransfer = async (from, to, amount) => {
    try {
        console.log('sending');
        const api = await ApiPromise.create({ provider: wsProvider });

        const { web3Accounts, web3Enable, web3FromAddress } = await import(
            "@polkadot/extension-dapp"
        );
        const extensions = await web3Enable("Polk4NET");

        const injector = await web3FromAddress(from);
        console.log(injector);

        api.tx.balances
            .transfer(to, amount * 1e12)
            .signAndSend(from, { signer: injector.signer }, txResHandler);

        // handleFetchTransaction(SENDER);
    } catch (error) {
        console.log(error);
    }
};

export const getBalance = async (wallet_address, web3Ctx) => {
    const api = await ApiPromise.create({ provider: wsProvider });

    const { nonce, data: balance } = await api.query.system.account(
      wallet_address
    );

    let balanceHuman = balance.toHuman().free;
    let floatBalance = parseFloat(balanceHuman.replace(/,/g, "")) / 1e12;
    web3Ctx.setBalance(Number(floatBalance.toFixed(4)));

    api.query.system.account(web3Ctx.data.wallet, ({ data: currentBalance, nonce: currentNonce }) => {
        let currentHuman = currentBalance.toHuman().free;
        let currentFloatBalance = parseFloat(currentHuman.replace(/,/g, "")) / 1e12;
        web3Ctx.setBalance(Number(currentFloatBalance.toFixed(4)));
    });
}