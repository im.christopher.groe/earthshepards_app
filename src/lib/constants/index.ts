export enum LAYOUT_OPTIONS {
  MODERN = 'modern',
  MINIMAL = 'minimal',
  RETRO = 'retro',
  CLASSIC = 'classic',
}

export const websocketURL = 'wss://www.shearnode.com';
export const graphqlApiURL = 'https://shearscan.com/api/v1/graphql/';
export const AIRDROP_API_URL = 'https://shearscan.com/shearapp/api/airdrop';
export const BACKEND_API_URL = 'https://shearscan.com/shearapp/api/';

// export const AIRDROP_API_URL = 'http://localhost:5050/shearapp/api/airdrop';
// export const BACKEND_API_URL = 'http://localhost:5050/shearapp/api/';