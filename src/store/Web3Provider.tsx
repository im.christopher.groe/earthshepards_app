"use client"
import { useReducer } from 'react';

import Web3Context from './web3-context';
import { getUserInfo } from '@/shared/api/user';

type Web3ContextType = {
  balance: number;
  data: null | {
    _id: string;
    role: string;
    name: string;
    email: string;
    wallet: string;
    avatar: string | null;
    company_name: string;
    company_detail: string;
    company_logo: string;
  };
  selectedProduct: null | any;
  setSelectedProduct: (data: any) => void;
  setBalance: (balance: number) => void;
  setUserData: (data: any) => void;
};

const defaultWeb3State = {
  balance: 0,
  data: null,
  selectedProduct: null,
};

const web3Reducer = (state: any, action: any) : any => {
  if(action.type === 'BALANCE') {
    return {
      balance: action.balance,
      data: state.data,
      selectedProduct: state.selectedProduct
    };
  }
  if(action.type === 'DATA') {
    return {
      balance: state.balance,
      data: action.data,
      selectedProduct: state.selectedProduct
    }
  }
  if(action.type === 'SELECT_PRODUCT') {
    return {
      balance: state.balance,
      data: state.data,
      selectedProduct: action.selectedProduct
    }
  }
  
  return defaultWeb3State;
};

const Web3Provider = (props: any) : any => {
  const [web3State, dispatchWeb3Action] = useReducer(web3Reducer, defaultWeb3State);
  
  // const setAccountHandler = async (account: string | null) => {
  //   dispatchWeb3Action({ type: 'ACCOUNT', account: account });
  //   if(account === null) {
  //     console.log('disconnect');
  //     dispatchWeb3Action({type: 'DATA', data: null});
  //   }
  // }

  // const loadAccountHandler = async () => {
  //   const { web3Accounts, web3Enable, web3FromAddress } = await import(
  //       "@polkadot/extension-dapp"
  //   );

  //   const extensions = await web3Enable("Polk4NET");
  //   if (extensions.length === 0) {
  //     return 1;
  //   }
  //   console.log('connecting');
  //   const account = await web3Accounts();

  //   if (account && account.length) {
  //     dispatchWeb3Action({type: 'ACCOUNT', account: account[0].address});
  //     const data = await getUserInfo(account[0].address);
  //     dispatchWeb3Action({type: 'DATA', data: data});
  //     if(data === null) {
  //       return 3;
  //     }
  //   } else {
  //     return 2;
  //   }

  //   return 0;
  // };

  const setUserDataHandler = async (data: any) => {
    dispatchWeb3Action({ type: 'DATA', data: data });
  }

  const setBalanceHandler = async (balance: number) => {
    dispatchWeb3Action({ type: 'BALANCE', balance: balance });
  }
  
  const setSelectedProductHandler = (data: any) => {
    dispatchWeb3Action({ type: 'SELECT_PRODUCT', selectedProduct: data });
  }

  const web3Context: Web3ContextType = {
    balance: web3State.balance,
    data: web3State.data,
    selectedProduct: web3State.selectedProduct,
    setSelectedProduct: setSelectedProductHandler,
    setBalance: setBalanceHandler,
    setUserData: setUserDataHandler,
  };
  
  return (
    <Web3Context.Provider value={web3Context}>
      {props.children}
    </Web3Context.Provider>
  );
};

export default Web3Provider;