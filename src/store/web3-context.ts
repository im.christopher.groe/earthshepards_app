import React from 'react';

type Web3ContextType = {
  balance: number;
  data: null | {
    _id: string;
    role: string;
    name: string;
    email: string;
    wallet: string;
    avatar: string | null;
    company_name: string;
    company_detail: string; 
    company_logo: string;
  };
  selectedProduct: null | any;
  setSelectedProduct: (data: any) => void;
  setUserData: (data: any) => void;
  setBalance: (balance: number) => void;
};

const Web3Context = React.createContext<Web3ContextType>({
  balance: 0,
  data: null,
  selectedProduct: null,
  setSelectedProduct: () => {},
  setBalance: () => {},
  setUserData: () => {},
});

export default Web3Context;