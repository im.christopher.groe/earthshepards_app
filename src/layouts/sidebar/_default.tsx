'use client';

import cn from 'classnames';
import AuthorCard from '@/components/ui/author-card';
import Logo from '@/components/ui/logo';
import { MenuItem } from '@/components/ui/collapsible-menu';
// import Scrollbar from '@/components/ui/scrollbar';
import Button from '@/components/ui/button';
import { useDrawer } from '@/components/drawer-views/context';
import { Close } from '@/components/icons/close';
import {
  defaultMenuItems,
  otherPagesMenuItems,
} from '@/layouts/sidebar/_menu-items';
import { LAYOUT_OPTIONS } from '@/lib/constants';
//images
import AuthorImage from '@/assets/images/author.jpg';
import React, { useContext } from 'react';
import Web3Context from '@/store/web3-context';

interface SidebarProps {
  className?: string;
  layoutOption?: string;
  menuItems?: any[];
}

export default function Sidebar({
  className,
  layoutOption = '',
  menuItems = defaultMenuItems,
}: SidebarProps) {
  const { closeDrawer } = useDrawer();
  const web3ctx = useContext(Web3Context);
  const sideBarMenus = menuItems?.map((item) => ({
    name: item.name,
    icon: item.icon,
    id: item.id ? item.id + '-1' : '',
    href:
      layoutOption +
      (layoutOption === `/${LAYOUT_OPTIONS.RETRO}` && item.href === '/'
        ? ''
        : item.href),
    ...(item.dropdownItems && {
      dropdownItems: item?.dropdownItems?.map((dropdownItem: any) => ({
        id: dropdownItem.id ? dropdownItem.id + '-1' : '',
        name: dropdownItem.name,
        ...(dropdownItem?.icon && { icon: dropdownItem.icon }),
        href: layoutOption + dropdownItem.href,
        role: dropdownItem.role
      })),
    }),
  }));

  return (
    <aside
      className={cn(
        'top-0 z-40 h-full w-full max-w-full border-dashed border-gray-200 bg-body ltr:left-0 ltr:border-r rtl:right-0 rtl:border-l dark:border-gray-700 dark:bg-dark xs:w-80 xl:fixed  xl:w-72 2xl:w-80',
        className
      )}
    >
      <div className="relative flex h-24 items-center justify-between overflow-hidden px-6 py-4 2xl:px-8">
        <Logo />
        <div className="md:hidden">
          <Button
            title="Close"
            color="white"
            shape="circle"
            variant="transparent"
            size="small"
            onClick={closeDrawer}
          >
            <Close className="h-auto w-2.5" />
          </Button>
        </div>
      </div>

      <div className="custom-scrollbar h-[calc(100%-98px)] overflow-hidden overflow-y-auto">
        <div className="px-6 pb-5 2xl:px-8">
          <AuthorCard />

          <div className="mt-12">
            {sideBarMenus?.map((item, index) => {
              if(item.name === 'Products' && (web3ctx.data === null || web3ctx.data.role === 'User')) { return ; }
              return (item.role === undefined || (item.role && web3ctx.data && item.role === web3ctx.data.role)) &&
                <MenuItem
                  key={'drawer' + item.name + index}
                  id={item.id}
                  name={item.name}
                  href={item.href}
                  icon={item.icon}
                  dropdownItems={item.dropdownItems}
                />;
              })}
            {otherPagesMenuItems?.map((item: any, index) => (
              <MenuItem
                key={'default' + item.name + index}
                id={item.id}
                name={item.name}
                href={item.href}
                icon={item.icon}
                dropdownItems={item.dropdownItems}
              />
            ))}
          </div>
        </div>
      </div>
    </aside>
  );
}
