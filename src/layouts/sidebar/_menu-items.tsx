import routes from '@/config/routes';
import { HomeIcon } from '@/components/icons/home';
import { FarmIcon } from '@/components/icons/farm';
import { PoolIcon } from '@/components/icons/pool';
import { ProfileIcon } from '@/components/icons/profile';
import { DiskIcon } from '@/components/icons/disk';
import { ExchangeIcon } from '@/components/icons/exchange';
import { VoteIcon } from '@/components/icons/vote-icon';
import { PlusCircle } from '@/components/icons/plus-circle';
import { CompassIcon } from '@/components/icons/compass';
import { LivePricing } from '@/components/icons/live-pricing';
import { LockIcon } from '@/components/icons/lock-icon';

export const defaultMenuItems = [
  // {
  //   name: 'Home',
  //   icon: <HomeIcon />,
  //   href: routes.home,
  // },
  {
    name: 'Mint Rewards',
    icon: <FarmIcon />,
    href: routes.products,
    id: 'help-mint-rewards',
  },
  {
    name: 'Network',
    icon: <LivePricing />,
    href: routes.livePricing,
  },
  // {
  //   name: 'Wallet',
  //   icon: <ExchangeIcon />,
  //   href: routes.wallet,
  // },
  // {
  //   name: 'Farm',
  //   icon: <FarmIcon />,
  //   href: routes.farms,
  // },
  // {
  //   name: 'Swap',
  //   icon: <ExchangeIcon />,
  //   href: routes.swap,
  // },
  // {
  //   name: 'Liquidity',
  //   icon: <PoolIcon />,
  //   href: routes.liquidity,
  // },
  {
    name: 'Products',
    icon: <CompassIcon />,
    href: routes.search,
    id: 'help-products',
    showRegisteredUser: true,
    dropdownItems: [
      // {
      //   name: 'Explore Products',
      //   icon: <CompassIcon />,
      //   href: routes.products,
      // },
      {
        name: 'Create Product',
        icon: <PlusCircle />,
        href: routes.createProduct,
        id: 'help-products-create',
        role: 'Company'
      },
      {
        name: 'My Products',
        icon: <DiskIcon />,
        href: routes.myProduct,
        id: 'help-products-myproducts',
        role: 'Company'
      },
      {
        name: 'Product Requests',
        icon: <DiskIcon />,
        href: routes.productRequests,
        role: 'Admin'
      },
      {
        name: 'Reward Requests',
        icon: <DiskIcon />,
        href: routes.rewardRequests,
        id: 'help-products-rewardrequests',
        role: 'Company'
      },
    ],
  },
  // {
  //   name: 'Profile',
  //   icon: <ProfileIcon />,
  //   href: routes.profile,
  // },
  // {
  //   name: 'Vote',
  //   icon: <VoteIcon />,
  //   href: routes.vote,
  //   dropdownItems: [
  //     {
  //       name: 'Explore',
  //       href: routes.vote,
  //     },
  //     {
  //       name: 'Vote with criptic',
  //       href: routes.proposals,
  //     },
  //     {
  //       name: 'Create proposal',
  //       href: routes.createProposal,
  //     },
  //   ],
  // },
];

export const otherPagesMenuItems = [
  // {
  //   name: 'Authentication',
  //   icon: <LockIcon className="w-[18px]" />,
  //   href: routes.signIn,
  //   dropdownItems: [
  //     {
  //       name: 'Sign in',
  //       href: routes.signIn,
  //     },
  //     {
  //       name: 'Sign up',
  //       href: routes.signUp,
  //     },
  //     {
  //       name: 'Reset pin',
  //       href: routes.resetPin,
  //     },
  //     {
  //       name: 'Forget password',
  //       href: routes.forgetPassword,
  //     },
  //   ],
  // },
];
