'use client';

import { useRouter } from 'next/navigation';
import cn from 'classnames';
import LogoIcon from '@/components/ui/logo-icon';
import { useWindowScroll } from '@/lib/hooks/use-window-scroll';
import { FlashIcon } from '@/components/icons/flash';
import Hamburger from '@/components/ui/hamburger';
import ActiveLink from '@/components/ui/links/active-link';
import SearchButton from '@/components/search/button';
import { useIsMounted } from '@/lib/hooks/use-is-mounted';
import { useDrawer } from '@/components/drawer-views/context';
import WalletConnect from '@/components/nft/wallet-connect';
import routes from '@/config/routes';
import { useLayout } from '@/lib/hooks/use-layout';
import { LAYOUT_OPTIONS } from '@/lib/constants';
import HelpButton from '@/components/help/button';
import Shepherd from 'shepherd.js';
import { useContext, useEffect, useState } from 'react';
import Web3Context from '@/store/web3-context';
import "./help.css";
import SignInButton from '@/components/auth/sign-in-button';
import JoinNowButton from '@/components/auth/join-now-button';
import { useModal } from '@/components/modal-views/context';
import { useLocalStorage } from '@/lib/hooks/use-local-storage';
import { isauthorized } from '@/shared/api/user';

const timeout = (delay: number) => {
  return new Promise( res => setTimeout(res, delay) );
}

const tour = new Shepherd.Tour({
  defaultStepOptions: {
    // cancelIcon: {
    //   enabled: true
    // },
    classes: 'shepherd-theme-dark',
    scrollTo: { behavior: 'smooth', block: 'center' }
  }
});

function NotificationButton() {
  const { layout } = useLayout();
  const isMounted = useIsMounted();
  return (
    isMounted && (
      <ActiveLink
        href={
          (layout === LAYOUT_OPTIONS.MODERN ? '' : `/${layout}`) +
          routes.notification
        }
      >
        <div className="relative flex h-10 w-10 shrink-0 cursor-pointer items-center justify-center rounded-full border border-gray-100 bg-white text-brand shadow-main transition-all hover:-translate-y-0.5 hover:shadow-large focus:-translate-y-0.5 focus:shadow-large focus:outline-none dark:border-gray-700 dark:bg-light-dark dark:text-white sm:h-12 sm:w-12">
          <FlashIcon className="h-auto w-3 sm:w-auto" />
          <span className="absolute right-0 top-0 h-2.5 w-2.5 rounded-full bg-brand shadow-light dark:bg-slate-50 sm:h-3 sm:w-3" />
        </div>
      </ActiveLink>
    )
  );
}

function HeaderRightArea() {
  return (
    <div className="relative order-last flex shrink-0 items-center gap-4 sm:gap-6 lg:gap-8">
      {/* <NotificationButton /> */}
      {/* <JoinNowButton /> */}
      <SignInButton />
    </div>
  );
}

export function RetroHeader({ className }: { className?: string }) {
  const router = useRouter();
  const isMounted = useIsMounted();
  const { openDrawer } = useDrawer();
  const windowScroll = useWindowScroll();
  const web3Ctx = useContext(Web3Context);

  useEffect(() => {
    if(web3Ctx.data) {
      if(window.innerWidth < 1280) {
        if(web3Ctx.data?.role === 'User') {
          tour.addStep({
            // title: 'Mint Rewards',
            text: `You can receive rewards here`,
            attachTo: {
              element: '#help-mint-rewards-1',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                async action() {
                  router.push('/retro/products');
                  await timeout(1000);
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });

          tour.addStep({
            // title: 'Mint Rewards',
            text: `Please click this button to request rewards`,
            attachTo: {
              element: '#mint-request-button',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                action() {
                  return this.cancel();
                },
                text: 'Done'
              }
            ],
            id: 'creating'
          });
        } else if (web3Ctx.data?.role === 'Company') {
          tour.addStep({
            // title: 'Products',
            text: `Please click here to manage your products`,
            attachTo: {
              element: '#help-products-1',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                action() {
                  var productsStatus = document.getElementById('help-products-1-status')?.value;
                  if(productsStatus == 'false') {
                    var products = document.getElementById('help-products-1');
                    products?.click();
                  }
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });
  
          tour.addStep({
            // title: 'Create Product',
            text: `Please click here to create product`,
            attachTo: {
              element: '#help-products-create-1',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                async action() {
                  router.push('/retro/products/create');
                  await timeout(1000);
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });

          tour.addStep({
            text: `Please upload product image`,
            attachTo: {
              element: '#create-product-image',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                action() {
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });
          
          tour.addStep({
            text: `Please input product name`,
            attachTo: {
              element: '#create-product-name',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                action() {
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });
          
          tour.addStep({
            text: `Please input product detail`,
            attachTo: {
              element: '#create-product-detail',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                action() {
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });
          
          tour.addStep({
            text: `Please input product price`,
            attachTo: {
              element: '#create-product-price',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                action() {
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });

          tour.addStep({
            text: `Please input product cut CO2 amount`,
            attachTo: {
              element: '#create-product-cut-co2-amount',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                action() {
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });

          tour.addStep({
            text: `Please upload sample bill document`,
            attachTo: {
              element: '#create-product-billDoc',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                action() {
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });

          tour.addStep({
            text: `Please click this Create button`,
            attachTo: {
              element: '#create-product-submit',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                async action() {
                  openDrawer('RETRO_SIDEBAR');
                  await timeout(1000);
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });

          tour.addStep({
            // title: 'My Products',
            text: `Please click here to see your products`,
            attachTo: {
              element: '#help-products-myproducts-1',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                async action() {
                  router.push('/retro/products/my-product');
                  await timeout(1000);
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });
          
          tour.addStep({
            // title: 'My Products',
            text: `The circles in top right cornor of product card represent the status of the product. Yellow is waiting the response from Admin. Green is accepted. Red is declined`,
            attachTo: {
              element: '#my-products-status',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                async action() {
                  openDrawer('RETRO_SIDEBAR');
                  await timeout(1000);
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });
          
          tour.addStep({
            // title: 'Reward Requests',
            text: `Please click here to manage reward requests from users`,
            attachTo: {
              element: '#help-products-rewardrequests-1',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                async action() {
                  router.push('/retro/products/reward-requests');
                  await timeout(1000);
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });

          tour.addStep({
            // title: 'Reward Requests',
            text: `You can manage reward requests for your products to click here`,
            attachTo: {
              element: '#reward-request-item-0',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                async action() {
                  openDrawer('RETRO_SIDEBAR');
                  await timeout(1000);
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });
          
          tour.addStep({
            // title: 'Reward Requests',
            text: `Please click here to get rewards`,
            attachTo: {
              element: '#help-mint-rewards-1',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                async action() {
                  router.push('/retro/products');
                  await timeout(1000);
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });
          
          tour.addStep({
            // title: 'Mint Rewards',
            text: `Please click 'Request' button to request rewards for selected product`,
            attachTo: {
              element: '#mint-request-button',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                action() {
                  return this.cancel();
                },
                text: 'Done'
              }
            ],
            id: 'creating'
          });
        }

      } else {
        if(web3Ctx.data?.role === 'User') {
          tour.addStep({
            // title: 'Mint Rewards',
            text: `You can receive rewards here`,
            attachTo: {
              element: '#help-mint-rewards',
              on: 'right'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                async action() {
                  router.push('/retro/products');
                  await timeout(1000);
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });

          tour.addStep({
            // title: 'Mint Rewards',
            text: `Please click this button to request rewards`,
            attachTo: {
              element: '#mint-request-button',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                action() {
                  return this.cancel();
                },
                text: 'Done'
              }
            ],
            id: 'creating'
          });
        } else if (web3Ctx.data?.role === 'Company') {
          tour.addStep({
            // title: 'Products',
            text: `Please click here to manage your products`,
            attachTo: {
              element: '#help-products',
              on: 'right'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                action() {
                  var productsStatus = document.getElementById('help-products-status')?.value;
                  if(productsStatus == 'false') {
                    var products = document.getElementById('help-products');
                    products?.click();
                  }
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });
  
          tour.addStep({
            // title: 'Create Product',
            text: `Please click here to create product`,
            attachTo: {
              element: '#help-products-create',
              on: 'right'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                async action() {
                  router.push('/retro/products/create');
                  await timeout(1000);
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });

          tour.addStep({
            text: `Please upload product image`,
            attachTo: {
              element: '#create-product-image',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                action() {
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });
          
          tour.addStep({
            text: `Please input product name`,
            attachTo: {
              element: '#create-product-name',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                action() {
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });
          
          tour.addStep({
            text: `Please input product detail`,
            attachTo: {
              element: '#create-product-detail',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                action() {
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });
          
          tour.addStep({
            text: `Please input product price`,
            attachTo: {
              element: '#create-product-price',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                action() {
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });

          tour.addStep({
            text: `Please input product cut CO2 amount`,
            attachTo: {
              element: '#create-product-cut-co2-amount',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                action() {
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });

          tour.addStep({
            text: `Please upload sample bill document`,
            attachTo: {
              element: '#create-product-billDoc',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                action() {
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });

          tour.addStep({
            text: `Please click this Create button`,
            attachTo: {
              element: '#create-product-submit',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                action() {
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });

          tour.addStep({
            // title: 'My Products',
            text: `Please click here to see your products`,
            attachTo: {
              element: '#help-products-myproducts',
              on: 'right'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                async action() {
                  router.push('/retro/products/my-product');
                  await timeout(1000);
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });
          
          tour.addStep({
            // title: 'My Products',
            text: `The circles in top right cornor of product card represent the status of the product. Yellow is waiting the response from Admin. Green is accepted. Red is declined`,
            attachTo: {
              element: '#my-products-status',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                action() {
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });
          
          tour.addStep({
            // title: 'Reward Requests',
            text: `Please click here to manage reward requests from users`,
            attachTo: {
              element: '#help-products-rewardrequests',
              on: 'right'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                async action() {
                  router.push('/retro/products/reward-requests');
                  await timeout(1000);
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });

          tour.addStep({
            // title: 'Reward Requests',
            text: `You can manage reward requests for your products to click here`,
            attachTo: {
              element: '#reward-request-item-0',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                action() {
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });
          
          tour.addStep({
            // title: 'Reward Requests',
            text: `Please click here to get rewards`,
            attachTo: {
              element: '#help-mint-rewards',
              on: 'right'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                async action() {
                  router.push('/retro/products');
                  await timeout(1000);
                  return this.next();
                },
                text: 'Next'
              }
            ],
            id: 'creating'
          });
          
          tour.addStep({
            // title: 'Mint Rewards',
            text: `Please click 'Request' button to request rewards for selected product`,
            attachTo: {
              element: '#mint-request-button',
              on: 'bottom'
            },
            buttons: [
              {
                action() {
                  return this.back();
                },
                classes: 'shepherd-button-secondary',
                text: 'Back'
              },
              {
                action() {
                  return this.cancel();
                },
                text: 'Done'
              }
            ],
            id: 'creating'
          });
        }
      }

    }
  }, [web3Ctx]);

  useEffect(() => {
    let auth_token = localStorage.getItem('auth_token');
    console.log(auth_token);
    if (auth_token) {
      isauthorized(web3Ctx);
    }
  }, []);

  return (
    <nav
      className={cn(
        'sticky top-0 z-30 h-16 w-full transition-all duration-300 ltr:right-0 rtl:left-0 sm:h-20 3xl:h-24',
        ((isMounted && windowScroll.y) as number) > 2
          ? 'bg-gradient-to-b from-white to-white/80 shadow-card backdrop-blur dark:from-dark dark:to-dark/80'
          : '',
        className
      )}
      style={{ fontFamily: 'Montserrat'}}
    >
      <div className="flex h-full items-center justify-between px-4 sm:px-6 lg:px-8 3xl:px-10 relative">
        <div className="w-full flex items-center mr-5">
          <div
            onClick={() => router.push('/')}
            className="flex items-center xl:hidden"
          >
            <LogoIcon />
          </div>
          <div className="mx-2 block sm:mx-4 xl:hidden">
            <Hamburger
              isOpen={false}
              variant="transparent"
              onClick={() => { openDrawer('RETRO_SIDEBAR') }}
              className="dark:text-white"
            />
          </div>
          <div className='w-full flex justify-between'>
            <SearchButton
              variant="transparent"
              className="ltr:-ml-[17px] rtl:-mr-[17px] dark:text-white"
            />
            {web3Ctx.data && <HelpButton
              variant="transparent"
              className="ltr:-ml-[17px] rtl:-mr-[37px] dark:text-white"
              onClick={async () => { 
                if(window.innerWidth < 1280) {
                  openDrawer('RETRO_SIDEBAR');
                  await timeout(1000);
                }
                tour.cancel(); 
                tour.start(); 
              }}
            />}
          </div>
        </div>
        <div className='absolute w-[85%] text-center text-gray-500 xs:hidden lg:block'>Please check how our marketplace is working here!</div>
        <HeaderRightArea />
      </div>
    </nav>
  );
}

export function ClassicHeader({ className }: { className?: string }) {
  const router = useRouter();
  const isMounted = useIsMounted();
  const { openDrawer } = useDrawer();
  const windowScroll = useWindowScroll();
  return (
    <nav
      className={cn(
        'sticky top-0 z-30 h-16 w-full transition-all duration-300 ltr:right-0 rtl:left-0 sm:h-20 3xl:h-24',
        ((isMounted && windowScroll.y) as number) > 2
          ? 'bg-gradient-to-b from-white to-white/80 shadow-card backdrop-blur dark:from-dark dark:to-dark/80'
          : '',
        className
      )}
    >
      <div className="flex h-full items-center justify-between px-4 sm:px-6 lg:px-8 3xl:px-10">
        <div className="flex items-center">
          <div
            onClick={() => router.push('/')}
            className="flex items-center xl:hidden"
          >
            <LogoIcon />
          </div>
          <div className="mx-2 block sm:mx-4 xl:hidden">
            <Hamburger
              isOpen={false}
              variant="transparent"
              onClick={() => openDrawer('CLASSIC_SIDEBAR')}
              className="dark:text-white"
            />
          </div>
          <SearchButton
            variant="transparent"
            className="ltr:-ml-[17px] rtl:-mr-[17px] dark:text-white"
          />
        </div>
        <HeaderRightArea />
      </div>
    </nav>
  );
}

export default function Header({ className }: { className?: string }) {
  const router = useRouter();
  const isMounted = useIsMounted();
  const { openDrawer } = useDrawer();
  const windowScroll = useWindowScroll();
  return (
    <nav
      className={cn(
        'sticky top-0 z-30 h-16 w-full transition-all duration-300 ltr:right-0 rtl:left-0 sm:h-20 3xl:h-24',
        ((isMounted && windowScroll.y) as number) > 2
          ? 'bg-gradient-to-b from-white to-white/80 shadow-card backdrop-blur dark:from-dark dark:to-dark/80'
          : '',
        className
      )}
    >
      <div className="flex h-full items-center justify-between px-4 sm:px-6 lg:px-8 3xl:px-10">
        <div className="flex items-center">
          <div
            onClick={() => router.push('/')}
            className="flex items-center xl:hidden"
          >
            <LogoIcon />
          </div>
          <div className="mx-2 block sm:mx-4 xl:hidden">
            <Hamburger
              isOpen={false}
              variant="transparent"
              onClick={() => openDrawer('DEFAULT_SIDEBAR')}
              className="dark:text-white"
            />
          </div>
          <SearchButton
            variant="transparent"
            className="ltr:-ml-[17px] rtl:-mr-[17px] dark:text-white"
          />
        </div>
        <HeaderRightArea />
      </div>
    </nav>
  );
}
