import { encodeAddress } from '@polkadot/util-crypto';

export const truncate = (address: string) => {
    return encodeAddress(address, 42).slice(0, 3) + '...' + encodeAddress(address, 42).slice(45);
}

export const truncateEncodedAddress = (address: string) => {
    return address.slice(0,6) + '...' + address.slice(42);
}

export const truncateReceiveWallet = (address: string) => {
    return encodeAddress(address, 42).slice(0, 13) + '...' + encodeAddress(address, 42).slice(35);
}

export const FormatTime = (time: string) => {
    time = time.replace('T', ' ').replace('+00:00', '');
    return time;
}

export const CheckInstallKiwiOnMobile = () => {
    // const isKiwiBrowser = () => {
    //     return /Kiwi/i.test(navigator.userAgent);
    // };
    // console.log(!!chrome, navigator.userAgentData)
    return isMobile() && !(!!chrome && navigator.userAgentData?.brands[2] === undefined);
}

export const isMobile = () => {
    if (
        /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Windows Phone/i.test(
          navigator.userAgent
        )
    ) {
        return true;
    }
    return false;
}