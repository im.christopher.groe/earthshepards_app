import Avatar from '@/components/ui/avatar';
import { useModal } from '@/components/modal-views/context';
import Button from '@/components/ui/button';
import Scrollbar from '@/components/ui/scrollbar';

export default function NoAccountAlert() {
  const { closeModal } = useModal();

  return (
    <div
      className="relative z-50 mx-auto h-[100px] w-[540px] max-w-full rounded-lg bg-white px-6 py-6 dark:bg-light-dark flex items-center justify-center"
    >
      <p>You have no account. Please add an account.</p>
    </div>
  );
}
