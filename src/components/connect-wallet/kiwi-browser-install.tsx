import Avatar from '@/components/ui/avatar';
import { useModal } from '@/components/modal-views/context';
import Button from '@/components/ui/button';
import Scrollbar from '@/components/ui/scrollbar';

export default function KiWiInstallModal() {
  const { closeModal } = useModal();

  return (
    <div
      className="relative z-50 mx-auto h-[400px] w-[540px] max-w-full rounded-lg bg-white px-6 py-6 dark:bg-light-dark flex items-center"
    >
      <div>
        <div className="ltr:pr-5 rtl:pl-5 text-center">
          Polkadot support Kiwi browser on mobile device only. <br/>
          Please install Kiwi browser (based on Chromium - Google Chrome) using below button.
        </div>
        <div className="text-center pt-8">
          <Button
            color="white"
            className="shadow-card ltr:ml-auto rtl:mr-auto md:h-10 md:px-5 xl:h-12 xl:px-7 bg-green-500 dark:bg-green-500"
            onClick={() => { window.location.href = 'https://play.google.com/store/apps/details?id=com.kiwibrowser.browser&hl=en_US&gl=US&pli=1' }}
          >
            Install Kiwi Browser
          </Button>
        </div>
      </div>
    </div>
  );
}
