import Avatar from '@/components/ui/avatar';
import { useModal } from '@/components/modal-views/context';
import Button from '@/components/ui/button';
import Scrollbar from '@/components/ui/scrollbar';

export default function ExtensionAlert() {
  const { closeModal } = useModal();

  return (
    <div
      className="relative z-50 mx-auto h-[400px] w-[540px] max-w-full rounded-lg bg-white px-6 py-6 dark:bg-light-dark flex items-center"
    >
      <div>
        <div className="ltr:pr-5 rtl:pl-5 text-center">
          Please connect Polkadotjs extension using Chrome Extension select.<br/>
          (Kiwi browser is also based on Chromium)<br/>
          After adding the Polkadot.js extension, please add account.
        </div>
        <div className="text-center pt-8">
          <Button
            color="white"
            className="shadow-card ltr:ml-auto rtl:mr-auto md:h-10 md:px-5 xl:h-12 xl:px-7 bg-green-500 dark:bg-green-500"
            onClick={() => { window.location.href = 'https://polkadot.js.org/extension/' }}
          >
            Get PolKaDot Extension
          </Button>
        </div>
      </div>
    </div>
  );
}
