import Avatar from '@/components/ui/avatar';
import { useModal } from '@/components/modal-views/context';
import Button from '@/components/ui/button';
import Scrollbar from '@/components/ui/scrollbar';
import { useContext, useState, useRef, useEffect } from 'react';
import Web3Context from '@/store/web3-context';
import { register, signin } from '@/shared/api/user';
import { uploadFile } from '@/shared/api/upload';
import AnchorLink from '../ui/links/anchor-link';
import Shepherd from 'shepherd.js';

const tour = new Shepherd.Tour({
  defaultStepOptions: {
    // cancelIcon: {
    //   enabled: true
    // },
    classes: 'shepherd-theme-dark',
    scrollTo: { behavior: 'smooth', block: 'center' }
  }
});

const timeout = (delay: number) => {
  return new Promise( res => setTimeout(res, delay) );
}

export default function SignInModal() {
  const { closeModal, openModal } = useModal();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const web3Ctx = useContext(Web3Context);

  useEffect(() => {
    tour.addStep({
      // title: 'Mint Rewards',
      text: `You can see tour to click this button`,
      attachTo: {
        element: '#help-button',
        on: 'bottom'
      },
      buttons: [
        {
          async action() {
            return this.cancel();
          },
          text: 'OK'
        }
      ],
      id: 'creating'
    });
  }, []);

  const SignInHandle = async () => {
    const data = {
      email, password
    };
    if(await signin(data, web3Ctx)) {
      closeModal();
      await timeout(1000);
      console.log(tour);
      tour.start();
    }
  }

  const gotoSignUpHandle = () => {
    console.log('goto signup')
    closeModal();
    openModal('REGISTER_MODAL');
  }

  return (
    <div
      className="relative z-50 mx-auto w-[540px] max-w-full rounded-lg bg-white sm:px-10 py-10 dark:bg-light-dark flex flex-col items-center"
    >
      <div className='flex flex-row m-2 w-[90%]'>
        <p className='flex items-center justify-end w-28 pr-3'>Email</p>
        <input type="text" 
          id="email" 
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
          placeholder="Email..." 
          required 
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          />
      </div>
      
      <div className='flex flex-row m-2 w-[90%]'>
        <p className='flex items-center justify-end w-28 pr-3'>Password</p>
        <input type="password" 
          id="password" 
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
          placeholder="" 
          required 
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          />
      </div>

      <div className='flex flex-row mt-5'>
        Need an entity account?
        <AnchorLink
          href={'#'}
          onClick={gotoSignUpHandle}
          className="ml-2 font-medium tracking-[0.5px] underline dark:text-green-400"
        >
          Sign Up
        </AnchorLink>
      </div>

      <div className="flex items-center text-center pt-8 border-t border-dashed border-gray-200 dark:border-gray-800">
        <Button
          className="shadow-card ltr:ml-auto rtl:mr-auto md:h-10 md:px-5 xl:h-12 xl:px-7 bg-[#007FCF] dark:bg-green-500"
          onClick={SignInHandle}
        >
          Sign In
        </Button>
      </div>
    </div>
  );
}
