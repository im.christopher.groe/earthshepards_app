import Avatar from '@/components/ui/avatar';
import { useModal } from '@/components/modal-views/context';
import Button from '@/components/ui/button';
import Scrollbar from '@/components/ui/scrollbar';
import { useContext, useState } from 'react';
import Web3Context from '@/store/web3-context';
import { register, updateProfile } from '@/shared/api/user';
import { uploadFile } from '@/shared/api/upload';

export default function EditProfileModal() {
  const { closeModal } = useModal();
  const web3Ctx = useContext(Web3Context);

  const [name, setName] = useState(web3Ctx.data ? web3Ctx.data.name : '');
  const [email, setEmail] = useState(web3Ctx.data ? web3Ctx.data.email : '');
  const [role, setRole] = useState(web3Ctx.data ? web3Ctx.data.role : 'User');
  const [companyName, setCompanyName] = useState(web3Ctx.data ? web3Ctx.data.company_name : '');
  const [companyDetail, setCompanyDetail] = useState(web3Ctx.data ? web3Ctx.data.company_detail : '');
  const [avatar, setAvatar] = useState('');
  const [companyLogo, setCompanyLogo] = useState('');

  const UpdateHandle = async () => {
    const data = {
      name, email, role, avatar,
      company_name: companyName,
      company_detail: companyDetail,
      company_logo: companyLogo
    };
    if(web3Ctx.data && await updateProfile(web3Ctx.data._id, data)) {
      // web3Ctx.loadAccount();
      closeModal();
    }
  }
  
  const handleAvatarChange = async (event: any) => {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      const body = new FormData();
      body.append("file", file);
      const res = await uploadFile(body);
      setAvatar(res);
    }
  };
  
  const handleLogoChange = async (event: any) => {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      const body = new FormData();
      body.append("file", file);
      const res = await uploadFile(body);
      setCompanyLogo(res);
    }
  };

  return (
    <div
      className="relative z-50 mx-auto w-[540px] max-w-full rounded-lg bg-white sm:px-10 py-10 dark:bg-light-dark flex flex-col items-center"
    >
      {/* <div className='flex flex-row m-2 w-[90%]'>
        <p className='flex items-center justify-end w-20 pr-3'>Avatar</p>
        <input 
          type="file" 
          id="avatar" 
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
          required 
          onChange={handleAvatarChange}
          />
      </div> */}
      <div className='flex flex-row m-2 w-[90%]'>
        <p className='flex items-center justify-end w-20 pr-3'>Name</p>
        <input 
          type="text" 
          id="name" 
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
          placeholder="Name..." 
          required 
          value={name}
          onChange={(e) => setName(e.target.value)}
          />
      </div>
      <div className='flex flex-row m-2 w-[90%]'>
        <p className='flex items-center justify-end w-20 pr-3'>Email</p>
        <input type="text" 
          id="email" 
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
          placeholder="Email..." 
          required 
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          />
      </div>

      <div className='flex flex-row m-2 w-[90%]'>
        <p className='flex items-center justify-end w-20 pr-3'>Role</p>
        <select 
          id="role" 
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
          value={role}
          onChange={(e) => {setRole(e.target.value)}}
        >
          <option value="User">User</option>
          <option value="Company">Company</option>
        </select>
      </div>
      
      {role === 'Company' && 
        <>
          <div className='flex flex-row m-2 w-[90%]'>
            <p className='flex items-center justify-end w-20 pr-3'>Company Name</p>
            <input type="text" 
              id="companyName" 
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
              placeholder="Company Name..." 
              required 
              value={companyName}
              onChange={(e) => setCompanyName(e.target.value)}
              />
          </div>

          {/* <div className='flex flex-row m-2 w-[90%]'>
            <p className='flex items-center justify-end w-20 pr-3'>Company Logo</p>
            <input 
              type="file" 
              id="companyLogo" 
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
              required 
              onChange={handleLogoChange}
              />
          </div> */}
          
          <div className='flex flex-row m-2 w-[90%]'>
            <p className='flex items-center justify-end w-20 pr-3'>Company Details</p>
            <input type="text" 
              id="companyDetails" 
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
              placeholder="Company Detail..." 
              required 
              value={companyDetail}
              onChange={(e) => setCompanyDetail(e.target.value)}
              />
          </div>
        </>
      }
      <div className="flex items-center text-center pt-8 border-t border-dashed border-gray-200 dark:border-gray-800">
        <Button
          className="shadow-card ltr:ml-auto rtl:mr-auto md:h-10 md:px-5 xl:h-12 xl:px-7 bg-[#007FCF] dark:bg-green-500"
          onClick={UpdateHandle}
        >
          Update
        </Button>
      </div>
    </div>
  );
}
