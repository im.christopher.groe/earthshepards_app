import Avatar from '@/components/ui/avatar';
import { useModal } from '@/components/modal-views/context';
import Button from '@/components/ui/button';
import Scrollbar from '@/components/ui/scrollbar';
import { useContext, useState, useRef, useEffect } from 'react';
import Web3Context from '@/store/web3-context';
import { register } from '@/shared/api/user';
import { uploadFile } from '@/shared/api/upload';
import AnchorLink from '../ui/links/anchor-link';
import Shepherd from 'shepherd.js';

const tour = new Shepherd.Tour({
  defaultStepOptions: {
    // cancelIcon: {
    //   enabled: true
    // },
    classes: 'shepherd-theme-dark',
    scrollTo: { behavior: 'smooth', block: 'center' }
  }
});

const timeout = (delay: number) => {
  return new Promise( res => setTimeout(res, delay) );
}

export default function RegisterModal() {
  const { closeModal, openModal } = useModal();
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [passwordConfirm, setPasswordConfirm] = useState('');
  const [email, setEmail] = useState('');
  const [role, setRole] = useState('');
  const [companyName, setCompanyName] = useState('');
  const [companyDetail, setCompanyDetail] = useState('');
  const [avatar, setAvatar] = useState('');
  const [companyLogo, setCompanyLogo] = useState('');

  const [step, setStep] = useState(0);
  const web3Ctx = useContext(Web3Context);

  useEffect(() => {
    tour.addStep({
      // title: 'Mint Rewards',
      text: `You can see tour to click this button`,
      attachTo: {
        element: '#help-button',
        on: 'bottom'
      },
      buttons: [
        {
          async action() {
            return this.cancel();
          },
          text: 'OK'
        }
      ],
      id: 'creating'
    });
  }, []);

  const RegisterHandle = async () => {
    const data = {
      name, email, password, passwordConfirm, role, avatar,
      company_name: companyName,
      company_detail: companyDetail,
      company_logo: companyLogo,
    };
    if(await register(data, web3Ctx)) {
      closeModal();
      await timeout(1000);
      console.log(tour);
    }
  }

  const handleAvatarChange = async (event: any) => {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      const body = new FormData();
      body.append("file", file);
      const res = await uploadFile(body);
      setAvatar(res);
    }
  };
  
  const handleLogoChange = async (event: any) => {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      const body = new FormData();
      body.append("file", file);
      const res = await uploadFile(body);
      setCompanyLogo(res);
    }
  };

  const gotoSignInHandle = () => {
    closeModal();
    openModal('SIGNIN_MODAL');
  }

  return (
    <div
      className="relative z-50 mx-auto w-[540px] max-w-full rounded-lg bg-white sm:px-10 py-10 dark:bg-light-dark flex flex-col items-center"
    >
      {step === 0 && <div className='m-2 w-[380px]'>
        <p className='flex items-center justify-center mb-5'>Please select your role</p>
        <div className='flex w-full justify-center'>
          <div className='mr-5'>
            <input type="radio" name="topping" value="Medium" id="medium" onClick={() => setRole('User')}/>
            <label htmlFor="medium"> User</label>
          </div>

          <div className='ml-5'>
            <input type="radio" name="topping" value="Regular" id="regular" onClick={() => setRole('Company')}/>
            <label htmlFor="regular"> Company</label>
          </div>
        </div>

        {/* <select 
          id="role" 
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
          value={role}
          onChange={(e) => {setRole(e.target.value)}}
        >
          <option value="User">User</option>
          <option value="Company">Company</option>
        </select> */}
      </div>}

      {step === 1 &&
        <>
          {/* <div className='flex flex-row m-2 w-[90%]'>
            <p className='flex items-center justify-end w-20 pr-3'>Avatar</p>
            <input 
              type="file" 
              id="avatar" 
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
              required 
              onChange={handleAvatarChange}
              />
          </div> */}
          <div className='flex flex-row m-2 w-[90%]'>
            <p className='flex items-center justify-end w-20 pr-3'>Name</p>
            <input 
              type="text" 
              id="name" 
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
              placeholder="Name..." 
              required 
              value={name}
              onChange={(e) => setName(e.target.value)}
              />
          </div>
          <div className='flex flex-row m-2 w-[90%]'>
            <p className='flex items-center justify-end w-20 pr-3'>Email</p>
            <input type="text" 
              id="email" 
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
              placeholder="Email..." 
              required 
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              />
          </div>
          
          <div className='flex flex-row m-2 w-[90%]'>
            <p className='flex items-center justify-end w-28 pr-3'>Password</p>
            <input type="password" 
              id="password" 
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
              placeholder="" 
              required 
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              />
          </div>

          <div className='flex flex-row m-2 w-[90%]'>
            <p className='flex items-center justify-end w-28 pr-3'>Password Confirm</p>
            <input type="password" 
              id="password_confirm" 
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
              placeholder="" 
              required 
              value={passwordConfirm}
              onChange={(e) => setPasswordConfirm(e.target.value)}
              />
          </div>
          
          {role === 'Company' && 
            <>
              <div className='flex flex-row m-2 w-[90%]'>
                <p className='flex items-center justify-end w-20 pr-3'>Company Name</p>
                <input type="text" 
                  id="companyName" 
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
                  placeholder="Company Name..." 
                  required 
                  value={companyName}
                  onChange={(e) => setCompanyName(e.target.value)}
                  />
              </div>

              {/* <div className='flex flex-row m-2 w-[90%]'>
                <p className='flex items-center justify-end w-20 pr-3'>Company Logo</p>
                <input 
                  type="file" 
                  id="companyLogo" 
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
                  required 
                  onChange={handleLogoChange}
                  />
              </div> */}
              
              <div className='flex flex-row m-2 w-[90%]'>
                <p className='flex items-center justify-end w-20 pr-3'>Company Details</p>
                <input type="text" 
                  id="companyDetails" 
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
                  placeholder="Company Detail..." 
                  required 
                  value={companyDetail}
                  onChange={(e) => setCompanyDetail(e.target.value)}
                  />
              </div>
            </>
          }
        </>
      }

      {/* {step === 2 && 
        <>
          <p className='text-center mt-5 mb-5'>
            After request, please wait until admin add your account on whitelist.
          </p>
        </>
      } */}

      <div className='flex flex-row mt-5'>
        Already have an account?
        <AnchorLink
          href={'#'}
          onClick={gotoSignInHandle}
          className="ml-2 font-medium tracking-[0.5px] underline dark:text-green-400"
        >
          Sign In
        </AnchorLink>
      </div>

      {step === 1
        ? <div className="flex items-center text-center pt-8 border-t border-dashed border-gray-200 dark:border-gray-800">
          <Button
            className="shadow-card ltr:ml-auto rtl:mr-auto md:h-10 md:px-5 xl:h-12 xl:px-7 bg-[#007FCF] dark:bg-green-500"
            onClick={RegisterHandle}
          >
            Register
          </Button>
        </div>
        : <div className="flex items-center text-center pt-8 border-t border-dashed border-gray-200 dark:border-gray-800">
          <Button
            className="shadow-card ltr:ml-auto rtl:mr-auto md:h-10 md:px-5 xl:h-12 xl:px-7 bg-[#007FCF] dark:bg-green-500"
            onClick={() => {
              if(role === ''){
                alert('Please select role');
              } else {
                // setStep((step + 1) % 3);
                setStep(1);
              }
            }}
          >
            Next
          </Button>
        </div>
      }
    </div>
  );
}
