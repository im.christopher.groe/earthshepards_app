import Button from '@/components/ui/button';
import { HelpIcon } from '@/components/icons/help';
import { useModal } from '@/components/modal-views/context';

export default function HelpButton({ ...props }) {
  const { openModal } = useModal();
  return (
    <Button
      shape="circle"
      aria-label="Help"
      onClick={() => props.onClick}
      id="help-button"
      {...props}
    >
      <HelpIcon className="h-auto w-3.5 sm:w-auto" />
    </Button>
  );
}
