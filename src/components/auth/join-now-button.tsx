'use client';
import { useContext, useEffect, useState } from 'react';
import { useWeb3Modal } from '@web3modal/react';
import { useAccount, useBalance, useDisconnect } from 'wagmi';
import cn from 'classnames';
import Button from '@/components/ui/button';
import { Menu } from '@/components/ui/menu';
import { Transition } from '@/components/ui/transition';
import ActiveLink from '@/components/ui/links/active-link';
import { ChevronForward } from '@/components/icons/chevron-forward';
import { PowerIcon } from '@/components/icons/power';
import Web3Context from '@/store/web3-context';
import { CheckInstallKiwiOnMobile, truncateEncodedAddress } from '@/utils/helper';
import { getBalance } from '@/shared/web3/chainInfo';
import { useModal } from '@/components/modal-views/context';

export default function JoinNowButton({
  btnClassName,
  anchorClassName,
}: {
  btnClassName?: string;
  anchorClassName?: string;
}) {
  const web3Ctx = useContext(Web3Context);
  const { openModal } = useModal();

  const connect = async () => {
    // const connectStatus: any = await web3Ctx.loadAccount();
    openModal('REGISTER_MODAL');
  }

  return (
    <>
      {!web3Ctx.data && <a
        onClick={() => connect()}
        className={cn('hover:cursor-pointer hover:text-green-400')}
      >
        Join Now
      </a>}
    </>
  );
}
