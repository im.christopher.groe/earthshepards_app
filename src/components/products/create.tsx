'use client';

import { useContext, useState } from 'react';
import cn from 'classnames';
import { Transition } from '@/components/ui/transition';
import { Listbox } from '@/components/ui/listbox';
import Button from '@/components/ui/button';
import { Switch } from '@/components/ui/switch';
import Input from '@/components/ui/forms/input';
import Textarea from '@/components/ui/forms/textarea';
import Uploader from '@/components/ui/forms/uploader';
import InputLabel from '@/components/ui/input-label';
import ToggleBar from '@/components/ui/toggle-bar';
import { ChevronDown } from '@/components/icons/chevron-down';
import { Ethereum } from '@/components/icons/ethereum';
import { Flow } from '@/components/icons/flow';
import { Warning } from '@/components/icons/warning';
import { Unlocked } from '@/components/icons/unlocked';
import React from 'react';
import Preview from '@/components/create-nft/nft-preview';
import PriceType from '@/components/create-nft/price-types-props';
import { createProduct } from '@/shared/api/product';
import Web3Context from '@/store/web3-context';
import { uploadFile } from '@/shared/api/upload';

const BlockchainOptions = [
  {
    id: 1,
    name: 'Ethereum',
    value: 'ethereum',
    icon: <Ethereum />,
  },
  {
    id: 2,
    name: 'Flow',
    value: 'flow',
    icon: <Flow />,
  },
]; 

export default function CreateProduct() {
  const [name, setName] = useState('');
  const [detail, setDetail] = useState('');
  const [price, setPrice] = useState(0);
  const [rewards, setRewards] = useState(0);
  const [image, setImage] = useState('');
  const [billDoc, setBillDoc] = useState('');
  const web3Ctx = useContext(Web3Context);

  const createHandler = async () => {
    if(name === '' || detail === '' || price === 0 || image === '' || billDoc === '') {
      alert('Please input all fields');
    } else {

      if (web3Ctx.data && web3Ctx.data.role === 'Company') {
        const data = {
          name, detail, price, image, reward_amount: rewards,
          user_id: web3Ctx.data._id, bill_doc: billDoc
        };
        if (await createProduct(data)) {
          // window.location.href = 'http://localhost:3000/retro/products/create';
          window.location.href = '/retro/products/create';
          // setName('');
          // setDetail('');
          // setPrice(0);
          // setRewards(0);
          // setImage('');
          // setBillDoc('');
        }
      }
    }
  }

  const handleImageChange = async (event: any) => {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      const body = new FormData();
      body.append("file", file);
      const res = await uploadFile(body);
      setImage(res);
    }
  };
  
  const handleBillDocChange = async (event: any) => {
    console.log(event.target.files[0]);
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      const body = new FormData();
      body.append("file", file);
      const res = await uploadFile(body);
      setBillDoc(res);
    }
  };

  return (
    <>
      <div className="mx-auto w-full pt-8 sm:pt-12 lg:px-8 xl:px-10 2xl:px-0">
        <div className="flex items-center justify-between">
          <h2 className="text-lg font-medium uppercase tracking-wider text-gray-900 dark:text-white sm:text-2xl" style={{ fontFamily: 'Montserrat'}}>
            Create New Product
          </h2>
        </div>
        <div className="mb-8 mt-6 grid grid-cols-1 gap-12 sm:mt-10">
          <div className="relative">
            {/* File uploader */}
            <div className="mb-8">
              <InputLabel title="Upload file" important />
              <input 
                type="file" 
                id="create-product-image" 
                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
                required 
                onChange={handleImageChange}
                />
            </div>
          </div>
        </div>

        {/* Name */}
        <div className="mb-8">
          <InputLabel title="Name" important />
          <Input id="create-product-name" type="text" placeholder="Product name..." value={name} onChange={(e) => {setName(e.target.value)}}/>
        </div>

        {/* Detail */}
        <div className="mb-8">
          <InputLabel
            title="Detail"
          />
          <Textarea id="create-product-detail" placeholder="Please type product details here..."  value={detail} onChange={(e) => {setDetail(e.target.value)}}/>
        </div>

        {/* <div className="mb-8">
          <ToggleBar
            title="Explicit &amp; Sensitive Content"
            subTitle="Set this item as explicit and sensitive content"
            icon={<Warning />}
            checked={explicit}
            onChange={() => setExplicit(!explicit)}
          />
        </div> */}

        {/* Price */}
        <div className="mb-8">
          <InputLabel title="Price" important />
          <Input
            min={0}
            type="number"
            placeholder="Enter your price..."
            inputClassName="spin-button-hidden"
            value={price}
            id="create-product-price"
            onChange={(e) => {setPrice(Number(e.target.value))}}
          />
        </div>
        
        {/* Reward */}
        <div className="mb-8">
          <InputLabel title="Cut CO2 Amount" />
          <Input
            min={0}
            type="number"
            placeholder="Enter reward shear token per unit..."
            inputClassName="spin-button-hidden"
            value={rewards} 
            id="create-product-cut-co2-amount"
            onChange={(e) => {setRewards(Number(e.target.value))}}
          />
        </div>
        
        {/* Bill Document Sample */}
        <div className="mb-8">
          <InputLabel
            title="Bill Document Sample"
            important
          />
          <input 
            type="file" 
            id="create-product-billDoc" 
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
            required 
            onChange={handleBillDocChange}
            />
        </div>

        {/* Blockchain */}
        {/* <div className="mb-8">
          <InputLabel title="Blockchain" />
          <div className="relative">
            <Listbox value={blockchain} onChange={setBlockChain}>
              <Listbox.Button className="text-case-inherit letter-space-inherit flex h-10 w-full items-center justify-between rounded-lg border border-gray-200 bg-white px-4 text-sm font-medium text-gray-900 outline-none transition-shadow duration-200 hover:border-gray-900 hover:ring-1 hover:ring-gray-900 dark:border-gray-700 dark:bg-gray-800 dark:text-gray-100 dark:hover:border-gray-600 dark:hover:ring-gray-600 sm:h-12 sm:px-5">
                <div className="flex items-center">
                  <span className="ltr:mr-2 rtl:ml-2">{blockchain.icon}</span>
                  {blockchain.name}
                </div>
                <ChevronDown />
              </Listbox.Button>
              <Transition
                leave="transition ease-in duration-100"
                leaveFrom="opacity-100"
                leaveTo="opacity-0"
              >
                <Listbox.Options className="absolute left-0 z-10 mt-1 grid w-full origin-top-right gap-0.5 rounded-lg border border-gray-200 bg-white p-1 shadow-large outline-none dark:border-gray-700 dark:bg-gray-800 xs:p-2">
                  {BlockchainOptions.map((option) => (
                    <Listbox.Option key={option.id} value={option}>
                      {({ selected }) => (
                        <div
                          className={`flex cursor-pointer items-center rounded-md px-3 py-2 text-sm text-gray-900 transition dark:text-gray-100  ${
                            selected
                              ? 'bg-gray-200/70 font-medium dark:bg-gray-600/60'
                              : 'hover:bg-gray-100 dark:hover:bg-gray-700/70'
                          }`}
                        >
                          <span className="ltr:mr-2 rtl:ml-2">
                            {option.icon}
                          </span>
                          {option.name}
                        </div>
                      )}
                    </Listbox.Option>
                  ))}
                </Listbox.Options>
              </Transition>
            </Listbox>
          </div>
        </div> */}

        <Button id="create-product-submit" shape="rounded" onClick={createHandler}>CREATE</Button>
      </div>
    </>
  );
}
