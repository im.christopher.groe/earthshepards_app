'use client';

import { useState, useEffect, useContext } from 'react';
import { getProducts, getProductsWithFilter } from '@/shared/api/product';
import cn from 'classnames';
import { motion } from 'framer-motion';
import { NormalGridIcon } from '@/components/icons/normal-grid';
import { CompactGridIcon } from '@/components/icons/compact-grid';
import { OptionIcon } from '@/components/icons/option';
import { useDrawer } from '@/components/drawer-views/context';
import Button from '@/components/ui/button';
import MyFeeds from './my-feeds';
import { SortList } from '@/components/search/filters';
import { useGridSwitcher } from '@/lib/hooks/use-grid-switcher';
import ProductFilters from './filters';
import Web3Context from '@/store/web3-context';

function GridSwitcher() {
  const { isGridCompact, setIsGridCompact } = useGridSwitcher();
  const { openDrawer, isOpen } = useDrawer();

  return (
    <div className="flex overflow-hidden rounded-lg">
      <button
        className={cn(
          'relative flex h-11 w-11 items-center justify-center bg-gray-100 transition dark:bg-gray-800',
          {
            'z-10 text-white': !isGridCompact,
            'text-brand dark:text-white': isGridCompact,
          }
        )}
        onClick={() => setIsGridCompact(!isGridCompact)}
        aria-label="Normal Grid"
      >
        {!isGridCompact && (
          <motion.span
            className="absolute bottom-0 left-0 right-0 h-full w-full bg-brand shadow-large"
            layoutId="gridSwitchIndicator"
          />
        )}
        <NormalGridIcon className="relative" />
      </button>
      <button
        className={cn(
          'relative flex h-11 w-11 items-center justify-center bg-gray-100 transition dark:bg-gray-800',
          {
            'z-10 text-white': isGridCompact,
            'text-brand dark:text-white': !isGridCompact,
          }
        )}
        onClick={() => setIsGridCompact(!isGridCompact)}
        aria-label="Normal Grid"
      >
        {isGridCompact && (
          <motion.span
            className="absolute bottom-0 left-0 right-0 h-full w-full  bg-brand shadow-large"
            layoutId="gridSwitchIndicator"
          />
        )}
        <CompactGridIcon className="relative" />
      </button>
      {/* <button
        className={cn(
          'relative flex h-11 w-11 items-center justify-center bg-gray-100 transition dark:bg-gray-800',
          { 'z-10 text-white': isOpen, 'text-brand dark:text-white': !isOpen }
        )}
        onClick={() => openDrawer('DRAWER_SEARCH')}
        aria-label="Filter"
      >
        {isOpen && (
          <motion.span
            className="absolute bottom-0 left-0 right-0 h-full w-full  bg-brand shadow-large"
            layoutId="gridSwitchIndicator"
          />
        )}
        <OptionIcon className="relative h-auto w-5" />
      </button> */}
    </div>
  );
}

export default function MyProducts() {
  const { openDrawer } = useDrawer();

  const [products, setProducts] = useState<any>([]);
  const web3Ctx = useContext(Web3Context);

  useEffect(() => {
    (async () => {
      const res = await getProductsWithFilter({ user_id: web3Ctx.data?._id });
      setProducts(res);
    })()
  }, []);

  return (
    <>
      <div className='flex flex-row'>
        {/* <div>
          <ProductFilters />
        </div> */}
        <div className="grid sm:pt-5 w-full">
          <div className="relative z-10 mb-6 sm:flex items-center justify-between">
            <span className="text-xs font-medium text-gray-900 dark:text-white sm:text-sm" style={{ fontFamily: 'Montserrat'}}>
              {products.length} Products
            </span>
            <div id="my-products-status" className="flex justify-end gap-2 3xl:gap-2 pt-5 sm:pt-0">
              <span style={{ backgroundColor: '#ee4', width: 20, height: 20, borderRadius: 10 }}> </span> Waiting
              <span className='ml-3' style={{ backgroundColor: '#4e4', width: 20, height: 20, borderRadius: 10 }}> </span> Accepted
              <span className='ml-3' style={{ backgroundColor: '#e44', width: 20, height: 20, borderRadius: 10 }}> </span> Declined
            </div>
            {/* <div className="flex gap-6 3xl:gap-8"> */}
              {/* <SortList /> */}
              {/* <div className="hidden 3xl:block">
                <GridSwitcher />
              </div> */}
              {/* <div className="hidden sm:flex sm:items-center sm:justify-center 3xl:hidden">
                <button
                  type="button"
                  onClick={() => openDrawer('DRAWER_SEARCH')}
                  className="hover:opacity-90 focus:opacity-90"
                >
                  <OptionIcon className="relative h-auto w-4" />
                </button>
              </div> */}
            {/* </div> */}
          </div>
          <MyFeeds className="sm:!grid-cols-1 md:!grid-cols-2 lg:!grid-cols-2 2xl:!grid-cols-3" />
          <div className="fixed bottom-6 left-1/2 z-10 w-full -translate-x-1/2 px-9 sm:hidden">
            <Button onClick={() => openDrawer('PRODUCT_SEARCH')} fullWidth>
              Filters
            </Button>
          </div>
        </div>
      </div>
    </>
  );
}
