import Avatar from '@/components/ui/avatar';
import { useModal } from '@/components/modal-views/context';
import Button from '@/components/ui/button';
import Scrollbar from '@/components/ui/scrollbar';
import { useContext, useState, useRef } from 'react';
import Web3Context from '@/store/web3-context';
import { register } from '@/shared/api/user';
import { uploadFile } from '@/shared/api/upload';
import Input from '../ui/forms/input';
import { rewardRequest } from '@/shared/api/reward';

export default function RewardModal() {
  const { closeModal } = useModal();
  const [amount, setAmount] = useState(0);
  const [doc, setDoc] = useState('');

  const web3Ctx = useContext(Web3Context);

  const RewardHandle = async () => {
    const data = {
      user_id: web3Ctx.data?._id,
      product_id: web3Ctx.selectedProduct._id,
      bill_doc: doc,
      amount
    };
    await rewardRequest(data)
    closeModal();
  }

  const handleDocChange = async (event: any) => {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      const body = new FormData();
      body.append("file", file);
      const res = await uploadFile(body);
      setDoc(res);
    }
  };

  return (
    <div
      className="relative z-50 mx-auto w-[540px] max-w-full rounded-lg bg-white sm:px-10 py-10 dark:bg-light-dark flex flex-col items-center"
    >
      <div className='flex flex-row m-2 w-[85%]'>
        <p className='flex items-center justify-end w-20 pr-3'>Bill Document</p>
        <input 
          type="file" 
          id="avatar" 
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" 
          required 
          onChange={handleDocChange}
          />
      </div><div className='flex flex-row m-2 w-[85%]'>
        <p className='flex items-center justify-end w-20 pr-3'>Amount</p>
        <Input
            min={0}
            type="number"
            placeholder="Enter amount..."
            inputClassName="spin-button-hidden"
            className='w-full'
            value={amount} 
            onChange={(e) => {setAmount(Number(e.target.value))}}
          />
      </div>
      <div className="flex items-center text-center pt-8 border-t border-dashed border-gray-200 dark:border-gray-800">
        <Button
          className="shadow-card ltr:ml-auto rtl:mr-auto md:h-10 md:px-5 xl:h-12 xl:px-7 bg-green-500 dark:bg-green-500"
          onClick={RewardHandle}
        >
          Request Rewards
        </Button>
      </div>
    </div>
  );
}
