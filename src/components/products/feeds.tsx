
import { useState, useEffect } from 'react';
import { getProducts, getProductsWithFilter } from '@/shared/api/product';
import cn from 'classnames';
import { NFTList } from '@/data/static/nft-list';
import ProductGrid from './product-card';
import { useGridSwitcher } from '@/lib/hooks/use-grid-switcher';

export default function Feeds({ className, products }: any) {
  const { isGridCompact } = useGridSwitcher();
  
  // const [products, setProducts] = useState<any>([]);

  // useEffect(() => {
  //   (async () => {
  //     const res = await getProductsWithFilter({ status: 'Accepted' });
  //     setProducts(res);
  //   })()
  // }, []);

  return (
    <div
      className={cn(
        'grid gap-5 grid-cols-1',
        // isGridCompact
        //   ? '3xl:!grid-cols-4 4xl:!grid-cols-5'
        //   : '3xl:!grid-cols-3 4xl:!grid-cols-4',
        className
      )}
    >
      {products.map((product: any, i: number) => (
        <ProductGrid
          key={i}
          data = {product} 
        />
      ))}
    </div>
  );
}
