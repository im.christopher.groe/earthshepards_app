import { useState } from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import CurrencySwapIcons from '@/components/ui/currency-swap-icons';
import { CoinList } from '@/components/ui/currency-swap-icons';
import TransactionInfo from '@/components/ui/transaction-info';
import { BACKEND_API_URL } from '@/lib/constants';

export default function RewardList({
  data, children, id
}: any) {
  let [isExpand, setIsExpand] = useState(false);
  // console.log(data.user_id)
  return (
    <div id={"reward-request-item-" + id} className="relative mb-3 overflow-hidden rounded-lg bg-white shadow-card transition-all last:mb-0 hover:shadow-large dark:bg-light-dark">
      <div
        className="relative grid h-auto cursor-pointer grid-cols-2 items-center gap-3 py-4 sm:h-20 sm:grid-cols-4 sm:gap-6 sm:py-0"
        onClick={() => setIsExpand(!isExpand)}
      >
        <div className="col-span-2 px-4 sm:col-auto sm:px-8 xl:px-4 flex items-center">
          {data.product_id.name}
        </div>
        <div className="col-span-2 px-4 sm:col-auto sm:px-8 xl:px-4 flex items-center">
          {data.user_id.avatar && data.user_id.avatar !== '' && <img src={BACKEND_API_URL + 'files/' + data.user_id.avatar} width={30} />} &nbsp;
          {data.user_id.name}
        </div>
        <div className="col-span-2 px-4 sm:col-auto sm:px-8 xl:px-4 flex items-center">
          {data.product_id.price} USD
        </div>
        <div className="col-span-2 px-4 sm:col-auto sm:px-8 xl:px-4 flex items-center">
          {data.amount}
        </div>
      </div>
      <AnimatePresence initial={false}>
        {isExpand && (
          <motion.div
            key="content"
            initial="collapsed"
            animate="open"
            exit="collapsed"
            variants={{
              open: { opacity: 1, height: 'auto' },
              collapsed: { opacity: 0, height: 0 },
            }}
            transition={{ duration: 0.4, ease: 'easeInOut' }}
          >
            <div className='p-5'>
              <div className="flex flex-row w-full">
                <div className='w-[30%]'>
                  <img
                    src={BACKEND_API_URL + 'files/' + data.product_id.image}
                    style={{
                      objectFit: 'cover',
                      height: '200px',
                      width: '100%'
                    }}
                    alt=""
                  />
                </div>
                <div className='p-5 flex flex-col justify-between'>
                  <div>
                    Details: {data.product_id.detail}
                  </div>
                  <div>
                    <a href={BACKEND_API_URL + "files/" + data.product_id.bill_doc} target='_blank' style={{ color: '#88f'}}> Sample bill document link </a>
                    <br />
                    <a href={BACKEND_API_URL + "files/" + data.bill_doc} target='_blank' style={{ color: '#88f'}}> Bill document link </a>
                  </div>
                </div>
              </div>
              {children}
            </div>
            {/* <div className="border-t border-dashed border-gray-200 px-4 py-4 dark:border-gray-700 sm:px-8 sm:py-6">
              <div className="mb-6 flex items-center justify-center rounded-lg bg-gray-100 p-3 text-center text-xs font-medium uppercase tracking-wider text-gray-900 dark:bg-gray-900 dark:text-white sm:h-13 sm:text-sm">
                get {from}/{to} lp tokens for staking
              </div>
              <div className="mb-6 grid grid-cols-1 gap-6 sm:grid-cols-2 lg:hidden">
                <div className="flex flex-col gap-3 sm:gap-4">
                  <TransactionInfo
                    label="Liquidity:"
                    value={liquidity}
                    className="text-xs sm:text-sm"
                  />
                  <TransactionInfo
                    label="Multiplier:"
                    value={multiplier}
                    className="text-xs sm:text-sm"
                  />
                </div>
              </div> */}
            {/* </div> */}
          </motion.div>
        )}
      </AnimatePresence>
    </div>
  );
}
