import { useState } from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import CurrencySwapIcons from '@/components/ui/currency-swap-icons';
import { CoinList } from '@/components/ui/currency-swap-icons';
import TransactionInfo from '@/components/ui/transaction-info';
import { BACKEND_API_URL } from '@/lib/constants';
import Input from '../ui/forms/input';
import Button from '../ui/button/button';
import { getProductsWithFilter, updateProduct } from '@/shared/api/product';

export default function ProductList({
  data, children, setProducts
}: any) {
  let [isExpand, setIsExpand] = useState(false);
  const [rewards, setRewards] = useState(data.reward_amount);

  
  const acceptHandle = async (id: any) => {
    await updateProduct(id, { status: "Accepted", reward_amount: rewards });
    const res = await getProductsWithFilter({ status: 'Waiting' });
    setProducts(res);
  }
  
  const denyHandle = async (id: any) => {
    await updateProduct(id, { status: "Declined", reward_amount: rewards });
    const res = await getProductsWithFilter({ status: 'Waiting' });
    setProducts(res);
  }


  return (
    <div className="relative mb-3 overflow-hidden rounded-lg bg-white shadow-card transition-all last:mb-0 hover:shadow-large dark:bg-light-dark">
      <div
        className="relative grid h-auto cursor-pointer grid-cols-2 items-center gap-3 py-4 sm:h-20 sm:grid-cols-3 sm:gap-6 sm:py-0"
        onClick={() => setIsExpand(!isExpand)}
      >
        <div className="col-span-2 px-4 sm:col-auto sm:px-8 xl:px-4 flex items-center">
          {data.user_id.company_logo != '' && <img src={BACKEND_API_URL + 'files/' + data.user_id.company_logo} width={30} />} &nbsp;
          {data.user_id.company_name}
        </div>
        <div className="col-span-2 px-4 sm:col-auto sm:px-8 xl:px-4 flex items-center">
          {/* <img src={'http://localhost:5050/api/files/' + data.image} width={50} /> &nbsp; */}
          {data.name}
        </div>
        <div className="col-span-2 px-4 sm:col-auto sm:px-8 xl:px-4 flex items-center">
          {data.price} USD
        </div>
      </div>
      <AnimatePresence initial={false}>
        {isExpand && (
          <motion.div
            key="content"
            initial="collapsed"
            animate="open"
            exit="collapsed"
            variants={{
              open: { opacity: 1, height: 'auto' },
              collapsed: { opacity: 0, height: 0 },
            }}
            transition={{ duration: 0.4, ease: 'easeInOut' }}
          >
            <div className='p-5'>
              <div className="flex flex-row w-full">
                <div className='w-[30%]'>
                  <img
                    src={BACKEND_API_URL + 'files/' + data.image}
                    style={{
                      objectFit: 'cover',
                      height: '200px',
                      width: '100%'
                    }}
                    alt=""
                  />
                </div>
                <div className='p-5 flex flex-col justify-between'>
                  <div>
                    Details: {data.detail}
                  </div>
                  <div className="col-span-2 sm:col-auto flex items-center">
                    Rewards: <Input className='w-[100px] m-2' type='number' value={rewards} onChange={(e) => setRewards(e.target.value)}></Input> SHE
                  </div>
                  <div>
                    <a href={BACKEND_API_URL + "files/" + data.bill_doc} target='_blank' style={{ color: '#88f'}}> Sample bill document link </a>
                  </div>
                </div>
              </div>
              {/* {children} */}
              <div className='flex justify-between pt-5'>
                <Button shape="rounded" className='w-[49%]' size="medium" onClick={() => acceptHandle(data._id)}>
                  APPROVE
                </Button>
                <Button shape="rounded" className='w-[49%] bg-red-500' size="medium" onClick={() => denyHandle(data._id)}>
                  Deny
                </Button>
              </div>
            </div>
            {/* <div className="border-t border-dashed border-gray-200 px-4 py-4 dark:border-gray-700 sm:px-8 sm:py-6">
              <div className="mb-6 flex items-center justify-center rounded-lg bg-gray-100 p-3 text-center text-xs font-medium uppercase tracking-wider text-gray-900 dark:bg-gray-900 dark:text-white sm:h-13 sm:text-sm">
                get {from}/{to} lp tokens for staking
              </div>
              <div className="mb-6 grid grid-cols-1 gap-6 sm:grid-cols-2 lg:hidden">
                <div className="flex flex-col gap-3 sm:gap-4">
                  <TransactionInfo
                    label="Liquidity:"
                    value={liquidity}
                    className="text-xs sm:text-sm"
                  />
                  <TransactionInfo
                    label="Multiplier:"
                    value={multiplier}
                    className="text-xs sm:text-sm"
                  />
                </div>
              </div> */}
            {/* </div> */}
          </motion.div>
        )}
      </AnimatePresence>
    </div>
  );
}
