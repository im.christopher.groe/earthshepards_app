
import { useState, useEffect, useContext } from 'react';
import { getProducts, getProductsWithFilter } from '@/shared/api/product';
import cn from 'classnames';
import { NFTList } from '@/data/static/nft-list';
import ProductGrid from './product-card';
import { useGridSwitcher } from '@/lib/hooks/use-grid-switcher';
import Web3Context from '@/store/web3-context';

export default function MyFeeds({ className }: { className?: string }) {
  const { isGridCompact } = useGridSwitcher();
  
  const [products, setProducts] = useState<any>([]);
  const web3Ctx = useContext(Web3Context);

  useEffect(() => {
    (async () => {
      const res = await getProductsWithFilter({ user_id: web3Ctx.data?._id });
      setProducts(res);
    })()
  }, []);

  return (
    <div
      className={cn(
        'grid gap-5 grid-cols-1',
        // isGridCompact
        //   ? '3xl:!grid-cols-4 4xl:!grid-cols-5'
        //   : '3xl:!grid-cols-3 4xl:!grid-cols-4',
        className
      )}
    >
      {products.map((product: any, i: number) => (
        <ProductGrid
          key={i}
          data = {product}
          showStatus = {true}
        />
      ))}
    </div>
  );
}
