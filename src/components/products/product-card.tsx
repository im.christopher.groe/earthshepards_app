'use client';

import Image from '@/components/ui/image';
import AnchorLink from '@/components/ui/links/anchor-link';
import { Verified } from '@/components/icons/verified';
import Avatar from '@/components/ui/avatar';
import { StaticImageData } from 'next/image';
import { useLayout } from '@/lib/hooks/use-layout';
import { BACKEND_API_URL, LAYOUT_OPTIONS } from '@/lib/constants';
import authorImage from '@/assets/images/author.jpg';
import routes from '@/config/routes';
import Button from '../ui/button/button';
import { useContext } from 'react';
import Web3Context from '@/store/web3-context';
import { useModal } from '../modal-views/context';

export default function ProductGrid({
  data, showStatus
}: any) {
  const { layout } = useLayout();
  const web3Ctx = useContext(Web3Context);
  const { openModal } = useModal();

  const requestHandler = () => {
    web3Ctx.setSelectedProduct(data);
    openModal('REWARD_REQUEST_MODAL');
  }

  return (
    <div className="relative overflow-hidden rounded-lg bg-white shadow-card transition-all duration-200 hover:shadow-large dark:bg-light-dark" style={{ height: '420px' }}>
      <div className="p-4"  style={{ fontFamily: 'Montserrat'}}>
        <AnchorLink
          href={''
            // (layout === LAYOUT_OPTIONS.MODERN ? '' : `/${layout}`) +
            // routes.profile
          }
          className="flex items-center text-sm font-medium text-gray-600 transition hover:text-gray-900 dark:text-gray-300 dark:hover:text-white"
        >
          {data.user_id.company_logo && data.user_id.company_logo !== '' 
            ? <img src={BACKEND_API_URL + 'files/' + data.user_id.company_logo} style={{ width: '30px', height: '30px' }}/>
            // : <Avatar
            //   image={authorImage}
            //   alt={data.user_id.company_name}
            //   className="dark:border-gray-400"
            // />
            : <></>
          }
          {/* <Avatar
            image={authorImage}
            alt={name}
            size="sm"
            className="text-ellipsis ltr:mr-3 rtl:ml-3 dark:border-gray-500"
          /> */}
          <span className="overflow-hidden text-ellipsis pl-2"> {data.user_id.company_name}</span>
        </AnchorLink>
        {showStatus && <span className='absolute top-4 right-4' style={{ backgroundColor: (data.status === 'Accepted' ? '#4e4' : (data.status === 'Declined' ? '#e44' : '#ee4')), width: 20, height: 20, borderRadius: 10 }}> </span>}
      </div>
      <AnchorLink
        href={''
          // (layout === LAYOUT_OPTIONS.MODERN ? '' : `/${layout}`) +
          // routes.nftDetails
        }
        className="relative block w-full"
      >
        {data.image && <img
          src={BACKEND_API_URL + 'files/' + data.image}
          style={{
            objectFit: 'cover',
            height: '220px',
            width: '100%'
          }}
          alt=""
        />}
      </AnchorLink>

      <div className="p-5 flex flex-col justify-between h-[150px]">
        <AnchorLink
          href={''
            // (layout === LAYOUT_OPTIONS.MODERN ? '' : `/${layout}`) +
            // routes.nftDetails
          }
          className="text-sm font-medium text-black dark:text-white"
        >
          Name: {data.name}
        </AnchorLink>
        {/* <div className="mt-1.5 flex">
          <AnchorLink
            href={''
              // (layout === LAYOUT_OPTIONS.MODERN ? '' : `/${layout}`) +
              // routes.nftDetails
            }
            className="inline-flex items-center text-xs text-gray-600 dark:text-gray-400"
          >
            {data.detail}
            <Verified className="ltr:ml-1 rtl:mr-1" />
          </AnchorLink>
        </div> */}
        <div className="mt-4 text-sm font-medium text-gray-900 dark:text-white">
          Price: {data.price} USD
        </div>
        <div className="mt-4 text-sm font-medium text-gray-900 dark:text-white">
          Rewards: {data.reward_amount} SHE
        </div>
        {web3Ctx.data && web3Ctx.data._id !== data.user_id._id && <div className='absolute bottom-3 right-2' >
          <Button id="mint-request-button" className='w-20' size='small' onClick={requestHandler}>Request</Button>
        </div>}
      </div>
    </div>
  );
}
