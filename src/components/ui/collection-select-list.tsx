import { useEffect, useState } from 'react';
import { StaticImageData } from 'next/image';
import { SearchIcon } from '@/components/icons/search';
import Avatar from '@/components/ui/avatar';
import CollectionImage1 from '@/assets/images/collection/collection-1.jpg';
import CollectionImage2 from '@/assets/images/collection/collection-2.jpg';
import CollectionImage3 from '@/assets/images/collection/collection-3.jpg';
import CollectionImage4 from '@/assets/images/collection/collection-4.jpg';
import { getCompanies } from '@/shared/api/user';
import { BACKEND_API_URL } from '@/lib/constants';
import { getProducts, getProductsWithFilter } from '@/shared/api/product';

export const collectionList = [
  {
    icon: CollectionImage1,
    name: 'Iron flower',
    value: 'iron-flower',
  },
  {
    icon: CollectionImage2,
    name: 'Creative web',
    value: 'creative-web',
  },
  {
    icon: CollectionImage3,
    name: 'Art in binary',
    value: 'art-in-binary',
  },
  {
    icon: CollectionImage4,
    name: 'Sound of wave',
    value: 'sound-of-wave',
  },
  {
    icon: CollectionImage2,
    name: 'Pixel art',
    value: 'pixel-art',
  },
];

interface CollectionSelectTypes {
  onSelect: (value: string) => void;
}

export default function CollectionSelect({ onSelect }: CollectionSelectTypes) {
  let [searchKeyword, setSearchKeyword] = useState('');
  
  const [companies, setCompanies] = useState<any>([]);
  const [data, setData] = useState<any>([]);
  // const [selected, setSelected] = useState<any>(null);

  useEffect(() => {
    (async () => {
      const res = await getCompanies();
      const _products = await getProductsWithFilter({ status: 'Accepted' });
      const temp: any = [];
      res.map((company: any) => {
        let count = _products.filter((x: any) => x.user_id._id === company._id).length;
        if(count > 0) {
          temp.push(company);
        }
      });
      setCompanies(temp);
      setData(temp);
    })()
  }, []);

  useEffect(() => {
    if (searchKeyword.length > 0) {
      const tmp = companies.filter(function (item: any) {
        const name = item.company_name;
        return (
          name.match(searchKeyword) ||
          (name.toLowerCase().match(searchKeyword) && name)
        );
      });
      setData(tmp);
    }
  }, [searchKeyword])
  
  function handleSelectedCompany(value: string) {
    onSelect(value);
    // setSelected(value);
  }
  return (
    <div className="w-full rounded-lg bg-white text-sm shadow-large dark:bg-light-dark">
      <div className="relative">
        <SearchIcon className="absolute left-6 h-full text-gray-700 dark:text-white" />
        <input
          type="search"
          autoFocus={true}
          onChange={(e) => setSearchKeyword(e.target.value)}
          placeholder="Search..."
          className="w-full border-x-0 border-b border-dashed border-gray-200 py-3.5 pl-14 pr-6 text-sm focus:border-gray-300 focus:ring-0 dark:border-gray-600 dark:bg-light-dark dark:text-white dark:focus:border-gray-500"
        />
      </div>
      <ul role="listbox" className="py-3">
        {data && data.length > 0 ? (
          data.map((item: any, index: any) => (
            <li
              key={index}
              role="listitem"
              tabIndex={index}
              onClick={() => handleSelectedCompany(item._id)}
              className={"mb-1 flex cursor-pointer items-center gap-3 py-1.5 px-6 outline-none hover:bg-gray-100 focus:bg-gray-200 dark:hover:bg-gray-700 dark:focus:bg-gray-600"}
            >
              {/* <Avatar image={item.icon} size="xs" alt={item.name} /> */}
              {item.company_logo != '' && <img src={BACKEND_API_URL + 'files/' + item.company_logo} style={{ width: '30px', height: '30px' }}/>}
              <span className="text-sm tracking-tight text-gray-600 dark:text-white">
                {item.company_name}
              </span>
            </li>
          ))
        ) : (
          // FIXME: need coin not found svg from designer
          <li className="px-6 py-5 text-center">
            <h3 className="mb-2 text-sm text-gray-600 dark:text-white">
              Ops! not found
            </h3>
          </li>
        )}
      </ul>
    </div>
  );
}
