'use client';

import cn from 'classnames';
import { useState, useRef, useEffect, useContext } from 'react';
import { motion } from 'framer-motion';
import { Tab, TabPanels, TabPanel } from '@/components/ui/tab';
import { ChevronDown } from '@/components/icons/chevron-down';
import { coinList } from '@/data/static/coin-list';
import Button from '@/components/ui/button';
import { IconUSFlag } from '@/components/icons/icon-us-flag';
import { useIsMounted } from '@/lib/hooks/use-is-mounted';
import { useBreakpoint } from '@/lib/hooks/use-breakpoint';
import CoinListBox from '@/components/ui/coin-listbox';
import { SwapIcon } from '@/components/icons/swap-icon';
import Web3Context from '@/store/web3-context';
import { truncateEncodedAddress } from '@/utils/helper';
import { airdrop } from '@/shared/api/airdrop';
import { sendTransfer } from '@/shared/web3/chainInfo';
import { useModal } from '../modal-views/context';
import { transfer } from '@/shared/api/user';

const tabMenu = [
  {
    title: 'Send',
    path: 'Send',
  },
  {
    title: 'Receive',
    path: 'Receive',
  },
  {
    title: 'Airdrop',
    path: 'Airdrop',
  },
];

function TabItem({
  children,
  className,
}: React.PropsWithChildren<{ className?: string }>) {
  return (
    <Tab
      className={({ selected }) =>
        cn(
          'relative z-0 uppercase tracking-wider hover:text-gray-900 focus:outline-none dark:hover:text-white',
          {
            'font-medium text-white hover:text-white focus:text-white':
              selected,
          },
          className
        )
      }
    >
      {({ selected }) => (
        <>
          <span className="flex w-full justify-between px-3 md:px-0">
            {children}
          </span>
          {selected && (
            <motion.span
              className={cn(
                'absolute bottom-0 left-0 right-0 -z-[1] h-full w-full rounded-lg bg-brand shadow-button'
              )}
              layoutId="activeTabIndicator-transact-coin"
            />
          )}
        </>
      )}
    </Tab>
  );
}

type CoinTransactionProps = {
  transactionType: string;
};

export function CoinTransaction({ transactionType }: CoinTransactionProps) {
  const web3Ctx = useContext(Web3Context);
  let [amount, setAmount] = useState<any>(0);
  const [firstCoin, setFirstCoin] = useState(coinList[0]);
  const [secondCoin, setSecondCoin] = useState(coinList[1]);
  const [conversionRate, setConversionRate] = useState(0);
  const [exchangeRate, setExchangeRate] = useState(0);
  const [toAddress, SetToAddress] = useState('');

  const { openModal } = useModal();

  let decimalPattern = /^[0-9]*[.,]?[0-9]*$/;
  let calcExRate = (firstCoin.price * amount) / secondCoin.price;

  const handleOnChangeFirstCoin = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    if (event.target.value.match(decimalPattern)) {
      setAmount(event.target.value);
      const price = amount * firstCoin.price;
      setConversionRate(price || 0);
    }
  };
  const handleOnChangeSecondCoin = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    if (event.target.value.match(decimalPattern)) {
      setAmount(event.target.value);
      const price = parseFloat(event.target.value) * secondCoin.price;
      setConversionRate(price || 0);
    }
  };

  useEffect(() => {
    const price = amount * firstCoin.price;
    setConversionRate(price || 0);
  }, [amount, firstCoin.price]);

  useEffect(() => {
    setExchangeRate(calcExRate);
  }, [amount, calcExRate]);

  const clickHandler = async () =>  {
    if(!web3Ctx.data) {
      openModal('SIGNIN_MODAL');
    } else if(transactionType === 'airdrop') {
      airdrop(web3Ctx.data.wallet);
    } else if(transactionType === 'send') {
      // sendTransfer(web3Ctx.account, toAddress, amount);
      await transfer({toAddress, amount});
    } else if(transactionType === 'receive') {
      navigator.clipboard.writeText(web3Ctx.data.wallet);
      alert('wallet address copied');
    }
  }

  return (
    <>
      {/* <div className="group relative flex rounded-lg border border-gray-200 transition-colors duration-200 hover:border-gray-900 dark:border-gray-700 dark:hover:border-gray-600">
        <CoinListBox
          coins={coinList}
          selectedCoin={firstCoin}
          setSelectedCoin={setFirstCoin}
        />

        <input
          type="text"
          value={amount}
          placeholder="0.0"
          inputMode="decimal"
          onChange={handleOnChangeFirstCoin}
          className="md w-full rounded-lg border-0 text-base outline-none focus:ring-0 ltr:text-right rtl:text-left dark:bg-light-dark"
        />
      </div> */}
      {/* <div className="relative mt-4 flex h-11 w-full items-center justify-between rounded-lg border border-gray-100 bg-body px-4 pl-3 text-sm text-gray-900 dark:border-gray-700 dark:bg-light-dark dark:text-white sm:h-13 sm:pl-4">
        <span className="relative flex items-center gap-3 font-medium">
          <IconUSFlag className="h-6 w-6 sm:h-[30px] sm:w-[30px]" /> USD
        </span>
        <span className="absolute top-0 h-full w-[1px] bg-gray-100 ltr:left-24 rtl:right-24 dark:bg-gray-700" />
        <span className="text-sm sm:text-base">
          {conversionRate.toFixed(4)}
        </span>
      </div> */}
      {transactionType === 'send' && (
        <>
          <div className="mt-4">
            <input
              type="text"
              placeholder="Wallet address"
              value={toAddress}
              onChange={(e) => SetToAddress(e.target.value)}
              className="h-11 w-full rounded-lg border border-gray-200 text-sm outline-none focus:border-gray-700 focus:ring-0 dark:border-gray-700 dark:bg-light-dark sm:h-13 sm:text-base"
            />
            <input
              type="text"
              value={amount}
              placeholder="0.0"
              inputMode="decimal"
              onChange={handleOnChangeFirstCoin}className="h-11 mt-3 w-full rounded-lg border border-gray-200 text-sm outline-none focus:border-gray-700 focus:ring-0 dark:border-gray-700 dark:bg-light-dark sm:h-13 sm:text-base"
            />
          </div>
          <Button
            size="large"
            shape="rounded"
            fullWidth={true}
            className="mt-6 uppercase xs:mt-8 xs:tracking-widest xl:px-2 2xl:px-9"
            onClick={clickHandler}
          >
            {web3Ctx.data ? 'Send' : 'Sign In'}
          </Button>
        </>
      )}
      {transactionType === 'receive' && (
        <>
          <div style={{ fontSize: 15 }}>
            <p className='text-center'>{web3Ctx.data ? 'Please copy your wallet address.' : 'Please sign in'}</p>
            {web3Ctx.data && <p className='text-center pt-5'>{truncateEncodedAddress(web3Ctx.data.wallet)}</p>}
          </div>

          <Button
            size="large"
            shape="rounded"
            fullWidth={true}
            className="mt-6 uppercase xs:mt-8 xs:tracking-widest xl:px-2 2xl:px-9"
            onClick={clickHandler}
          >
            {web3Ctx.data ? 'Copy' : 'Sign In'}
          </Button>
        </>
      )}
      {transactionType === 'airdrop' && (
        <>
          <div style={{ fontSize: 15 }}>
            <p className='text-center'>Airdrop started at Aug 3.</p>
            <p className='text-center'>9995 airdrop remains</p>
            <br/>
            <p className='text-center'>Airdroop 4 SHE($20)</p>
            <p className='text-center'>1SHE = $5</p>
          </div>

          <Button
            size="large"
            shape="rounded"
            fullWidth={true}
            className="mt-6 uppercase xs:mt-8 xs:tracking-widest xl:px-2 2xl:px-9"
            onClick={clickHandler}
          >
            {web3Ctx.data ? 'Airdrop' : 'Sign In'}
          </Button>
        </>
      )}

      
    </>
  );
}

export function CoinConverter() {
  let [amount, setAmount] = useState<any>(0);
  const [firstCoin, setFirstCoin] = useState(coinList[0]);
  const [conversionRate, setConversionRate] = useState(0);
  let [toggleCoin, setToggleCoin] = useState(false);
  let decimalPattern = /^[0-9]*[.,]?[0-9]*$/;

  const handleOnChangeFirstCoin = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    if (event.target.value.match(decimalPattern)) {
      setAmount(event.target.value);
      const price = amount * firstCoin.price;
      setConversionRate(price || 0);
    }
  };

  useEffect(() => {
    const price = amount * firstCoin.price;
    setConversionRate(price || 0);
  }, [amount, firstCoin.price]);

  return (
    <>
      <div
        className={cn(
          'relative mt-8 flex gap-4 px-8 pb-10',
          toggleCoin ? 'flex-col-reverse' : 'flex-col'
        )}
      >
        <div className="group relative flex rounded-lg border border-gray-200 transition-colors duration-200 hover:border-gray-900 dark:border-gray-700 dark:hover:border-gray-600">
          <CoinListBox
            coins={coinList}
            selectedCoin={firstCoin}
            setSelectedCoin={setFirstCoin}
          />

          <input
            type="text"
            value={amount}
            placeholder="0.0"
            inputMode="decimal"
            onChange={handleOnChangeFirstCoin}
            className="md w-full rounded-lg border-0 text-base outline-none focus:ring-0 ltr:text-right rtl:text-left dark:bg-light-dark"
          />
        </div>
        <div className="absolute left-1/2 top-1/2 z-[1] -ml-4 -mt-9 rounded-full bg-white shadow-large dark:bg-gray-600">
          <Button
            size="mini"
            color="gray"
            shape="circle"
            variant="transparent"
            onClick={() => setToggleCoin(!toggleCoin)}
          >
            <SwapIcon className="h-auto w-3" />
          </Button>
        </div>
        <div className="relative mt-1 flex h-11 w-full items-center justify-between rounded-lg border border-gray-100 bg-body px-4 pl-3 text-sm text-gray-900 dark:border-gray-700 dark:bg-light-dark dark:text-white sm:h-13 sm:pl-4">
          <span className="relative flex items-center gap-3 font-medium">
            <IconUSFlag className="h-6 w-6 sm:h-[30px] sm:w-[30px]" /> USD
          </span>
          <span className="absolute top-0 h-full w-[1px] bg-gray-100 ltr:left-24 rtl:right-24 dark:bg-gray-700" />
          <span className="text-sm sm:text-base">
            {conversionRate.toFixed(4)}
          </span>
        </div>
      </div>
    </>
  );
}

export default function TransactCoin({
  className,
}: React.PropsWithChildren<{ className?: string }>) {
  const isMounted = useIsMounted();
  const breakpoint = useBreakpoint();
  const dropdownEl = useRef<HTMLDivElement>(null);
  let [selectedTabIndex, setSelectedTabIndex] = useState(0);
  let [visibleMobileMenu, setVisibleMobileMenu] = useState(false);

  return (
    <div className={cn(className)}>
      <Tab.Group
        selectedIndex={selectedTabIndex}
        onChange={(index) => setSelectedTabIndex(index)}
      >
        <Tab.List className="relative mb-6 text-sm uppercase sm:gap-8 sm:rounded-none" style={{ fontFamily: 'Montserrat'}}>
          {isMounted &&
          ['xs', 'sm', 'md', 'lg', 'xl'].indexOf(breakpoint) !== -1 ? (
            <div
              ref={dropdownEl}
              className="rounded-lg border-2 border-gray-200 dark:border-gray-700"
            >
              <button
                onClick={() => setVisibleMobileMenu(!visibleMobileMenu)}
                className="flex w-full items-center justify-between px-4 py-2.5 uppercase text-gray-400 dark:text-gray-300 sm:px-5 sm:py-3.5 hover:bg-white"
              >
                <span className="font-medium text-gray-900 dark:text-gray-100">
                  {tabMenu[selectedTabIndex].title}
                </span>
                <ChevronDown className="h-auto w-3.5" />
              </button>
              <div
                className={cn(
                  'absolute left-0 top-full z-10 mt-1 grid w-full gap-0.5 rounded-lg border border-gray-200 bg-white p-2 text-left shadow-large dark:border-gray-700 dark:bg-gray-800 xs:gap-1',
                  visibleMobileMenu
                    ? 'visible opacity-100'
                    : 'invisible opacity-0'
                )}
              >
                {tabMenu.map((item) => (
                  <div
                    key={item.path}
                    onClick={() => setVisibleMobileMenu(false)}
                  >
                    <TabItem className="w-full p-2.5 hover:bg-white">{item.title}</TabItem>
                  </div>
                ))}
              </div>
            </div>
          ) : (
            <div className="flex gap-2 2xl:gap-0.5 3xl:gap-2">
              {tabMenu.map((item) => (
                <TabItem key={item.path} className="px-3 py-[5px] hover:bg-white">
                  {item.title}
                </TabItem>
              ))}
            </div>
          )}
        </Tab.List>
        <span className="my-6 block h-[1px] border-b border-dashed border-b-gray-200 dark:border-b-gray-700"></span>
        <TabPanels>
          <TabPanel className="focus:outline-none">
            <CoinTransaction transactionType="send" />
          </TabPanel>
          <TabPanel className="focus:outline-none">
            <CoinTransaction transactionType="receive" />
          </TabPanel>
          <TabPanel className="focus:outline-none">
            <CoinTransaction transactionType="airdrop" />
          </TabPanel>
        </TabPanels>
      </Tab.Group>
    </div>
  );
}
