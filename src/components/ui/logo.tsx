'use client';

import Image from '@/components/ui/image';
import AnchorLink from '@/components/ui/links/anchor-link';
import { useIsMounted } from '@/lib/hooks/use-is-mounted';
import { useIsDarkMode } from '@/lib/hooks/use-is-dark-mode';
import { useLayout } from '@/lib/hooks/use-layout';
import logo from '@/assets/images/logo.png';
import lightLogo from '@/assets/images/logo.svg';
import darkLogo from '@/assets/images/logo-white.svg';
import routes from '@/config/routes';
import { LAYOUT_OPTIONS } from '@/lib/constants';
import cn from 'classnames';

interface LogoPropTypes {
  className?: string;
}

export default function Logo({ className }: LogoPropTypes) {
  const { layout } = useLayout();
  const isMounted = useIsMounted();
  const { isDarkMode } = useIsDarkMode();
  return (
    isMounted && (
      <AnchorLink
        href={{
          pathname: '/',
        }}
        className={cn('flex', className)}
      >
        <div className="relative flex items-center justify-center overflow-hidden pt-5">
          <Image src={logo} alt="Earthshepards" width={184} priority />
          {/* {isDarkMode && (
            <Image src={darkLogo} alt="Criptic" height={24} priority />
          )}
          {!isDarkMode && (
            <Image src={lightLogo} alt="Criptic" height={24} priority />
          )} */}
        </div>
      </AnchorLink>
    )
  );
}
