import Avatar from '@/components/ui/avatar';
import { StaticImageData } from 'next/image';
import { useContext } from 'react';
import Web3Context from '@/store/web3-context';
import WalletImage from '@/assets/images/author.png';
import Button from './button/button';
import { useModal } from '../modal-views/context';
import { BACKEND_API_URL } from '@/lib/constants';

export default function AuthorCard() {
  const web3Ctx = useContext(Web3Context);
  const { openModal } = useModal();
  
  const RegisterHandle = () => {
    // if(!web3Ctx.account) {
    //   alert('Please connect wallet');
    //   return;
    // }
    openModal('REGISTER_MODAL');
  }

  return (
    <div
      className={`flex items-center rounded-lg  ${
        (web3Ctx.data && web3Ctx.data.name)
          ? 'bg-gray-100  p-5  dark:bg-light-dark'
          : 'ml-3 justify-center bg-none p-5 dark:mr-3 dark:bg-none'
      }`}
    >
      {web3Ctx.data && web3Ctx.data.avatar && web3Ctx.data.avatar !== '' 
        ? <img src={BACKEND_API_URL + 'files/' + web3Ctx.data.avatar} width={40}/>
        : <Avatar
          image={WalletImage}
          alt={web3Ctx.data && web3Ctx.data.name ? web3Ctx.data.name : ''}
          className="dark:border-gray-400"
        />
      }
      
      <div className="ltr:pl-6 rtl:pr-3">
        <h3 className="text-sm font-medium uppercase tracking-wide text-gray-900 dark:text-white" style={{ fontFamily: 'Montserrat'}}>
          {web3Ctx.data && web3Ctx.data.name ? web3Ctx.data.name : 'Unregistered'}
        </h3>
        {web3Ctx.data && web3Ctx.data.role 
          ? <span className="mt-1 block text-xs text-gray-600 dark:text-gray-400">
              {web3Ctx.data.role}
            </span>
          : <></> // <Button style={{ width: '90px', height: '25px' }} onClick={RegisterHandle}> Register </Button>
        }
      </div>
    </div>
  );
}
